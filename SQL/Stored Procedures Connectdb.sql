DROP procedure IF EXISTS `sp_Users`;
DELIMITER $$
CREATE DEFINER=`admin`@`%`PROCEDURE `sp_Users` (
	IN pOpc						char(3),
	IN pIdUser 					bigint,
	IN pIdPlan 					int,
	IN pFirstNameUser 			varchar(30),
	IN pLastNameUser 			varchar(30),
	IN pEmailUser 				varchar(60),
	IN pPasswordUser 			varchar(60),
	IN pLicenseTypeUser			varchar(30),
	IN pCountryUser 			varchar(80),
	IN pCardNumberUser 			varchar(30),
	IN pMonthUser 				int,
	IN pYearUser 				int, 
    IN pSecurityNumberUser		varchar(5),
    IN pCardholderUser			varchar(100),
    IN pStatusUser				boolean
    
   /* `Id_User` bigint NOT NULL AUTO_INCREMENT,
  `Id_Plan` int NOT NULL,
  `FirstName_User` varchar(30) NOT NULL,
  `LastName_User` varchar(30) NOT NULL,
  `Email_User` varchar(60) NOT NULL,
  `Password_User` varchar(60) NOT NULL,
  `LicensceType_Billing` varchar(10) NOT NULL,
  `Country_User` varchar(80) NOT NULL,
  `CardNumber_User` varchar(30) NOT NULL,
  `Month_User` int NOT NULL,
  `Year_User` int NOT NULL,
  `SecurityNumber_User` varchar(5) NOT NULL,
  `Cardholder_User` varchar(100) NOT NULL,
  `Status_User` boolean NOT NULL DEFAULT '1', */
)
BEGIN
	IF pOpc = 'I'
	THEN 
		INSERT INTO users(	Id_Plan, FirstName_User, LastName_User, Email_User,
								Password_User, LicenseType_User, Country_User, CardNumber_User, Month_User, Year_User, SecurityNumber_User, Cardholder_User)
			VALUES(	pIdPlan, pFirstNameUser, pLastNameUser, pEmailUser, pPasswordUser, pLicenseTypeUser, pCountryUser, pCardNumberUser,
					pMonthUser, pYearUser, pSecurityNumberUser, pCardholderUser);
	END IF;
	IF pOpc = 'U'
	THEN
		UPDATE users
			SET Id_Plan = pIdPlan,
				FirstName_User = pFirstNameUser,				
                LastName_User = pLastNameUser,
				Email_User = pEmailUser,
                Password_User = pPasswordUser,
				LicenseType_User = pLicenseTypeUser,
                Country_User = pCountryUser,
				CardNumber_User = pCardNumberUser,
                Month_User = pMonthUser,
                Year_User = pYearUser,
                SecurityNumber_User = pSecurityNumberUser,
                Cardholder_User = pCardholderUser
			WHERE Id_User = pIdUser;
	END IF;
    
	IF pOpc = 'D'
	THEN 
		DELETE
			FROM users
            WHERE Id_User = pIdUser;
	END IF;


	IF pOpc = 'LC'
	THEN
		UPDATE users
			SET Status_User = pStatusUser
		WHERE Id_User = pIdUser;
	END IF;
    
    IF pOpc = 'DU'
    THEN
    
		UPDATE users
			SET Id_Plan = pIdPlan,
				FirstName_User = pFirstNameUser,
                LastName_User = pLastNameUser,
                Password_User = pPasswordUser,
                Country_User = pCountryUser,
				CardNumber_User = pCardNumberUser,
                Month_User = pMonthUser,
                Year_User = pYearUser,
                SecurityNumber_User = pSecurityNumberUser,
                Cardholder_User = pCardholderUser
			WHERE Id_User = pIdUser;  
    END IF;
    
	IF pOpc = 'X'
	THEN
		SELECT Id_User, Id_Plan, FirstName_User, LastName_User, Email_User, 
				Password_User, LicenseType_User, Country_User, 
                CardNumber_User, Month_User, Year_User, SecurityNumber_User, Cardholder_User,
                Status_User
			FROM users;
	END IF;
    
    IF pOpc = 'S'
    THEN
		SELECT Id_User, Id_Plan, FirstName_User, LastName_User, Email_User, 
				Password_User, LicenseType_User, Country_User, 
                CardNumber_User, Month_User, Year_User, SecurityNumber_User, Cardholder_User,
                Status_User
			FROM users
		WHERE Id_User = pIdUser;
    END IF;
    
    IF pOpc = 'L'
	THEN
		SELECT U.Id_User, U.FirstName_User, U.LastName_User, U.Email_User, 
				U.Password_User, U.LicenseType_User, U.Country_User, 
                U.CardNumber_User, U.Month_User, U.Year_User, U.SecurityNumber_User, U.Cardholder_User,
                U.Status_User, U.Id_Plan, P.Name_Plan, P.Type_Plan, P.Price_Plan, P.Status_Plan
			FROM users as U
		INNER JOIN Plans AS P
        ON U.Id_Plan = P.Id_Plan
		WHERE Email_User = pEmailUser
        AND Password_User = pPasswordUser;
    
    END IF;
    
    IF pOpc = 'DH'
	THEN
		SELECT U.Id_User, U.FirstName_User, U.LastName_User, U.Email_User, 
				U.Password_User, U.LicenseType_User, U.Country_User, 
                U.CardNumber_User, U.Month_User, U.Year_User, U.SecurityNumber_User, U.Cardholder_User,
                U.Status_User, U.Id_Plan, P.Name_Plan, P.Type_Plan, P.Price_Plan, P.Status_Plan, PY.Id_Payment Id_Payment, PY.CutoffDate_Payment
			FROM users as U
		INNER JOIN plans AS P
        ON U.Id_Plan = P.Id_Plan
        INNER JOIN payments AS PY
        ON U.Id_user = PY.Id_User
		WHERE U.Id_User = pIdUser
        ORDER BY PY.Id_Payment DESC LIMIT 1;
    
    END IF;
    
    IF pOpc = 'CPU'
	THEN
		SELECT U.Id_User, U.FirstName_User, U.LastName_User, U.Email_User, 
				U.Password_User, U.LicenseType_User, U.Country_User, 
                U.CardNumber_User, U.Month_User, U.Year_User, U.SecurityNumber_User, U.Cardholder_User,
                U.Status_User, U.Id_Plan, P.Name_Plan, P.Type_Plan, P.Price_Plan, P.Status_Plan
			FROM users as U
		INNER JOIN Plans AS P
        ON U.Id_Plan = P.Id_Plan
		WHERE Id_User = pIdUser;
    
    END IF;
    
	IF pOpc = 'CE'
	THEN
		SELECT Id_User, Id_Plan, FirstName_User, LastName_User, Email_User, 
				Password_User, LicenseType_User, Country_User, 
                CardNumber_User, Month_User, Year_User, SecurityNumber_User, Cardholder_User,
                Status_User
			FROM users
		WHERE Email_User = pEmailUser;
    END IF;
    
    IF pOpc = 'UP' #DEPRECATED
    THEN
		SELECT U.Id_User, U.FirstName_User, U.LastName_User, U.Email_User, 
				U.Password_User, U.LicenseType_User, U.Country_User, 
                U.CardNumber_User, U.Month_User, U.Year_User, U.SecurityNumber_User, U.Cardholder_User,
                U.Status_User, U.Id_Plan, P.Name_Plan, P.Type_Plan, P.Price_Plan, P.Status_Plan
			FROM users as U
		INNER JOIN Plans AS P
        ON U.Id_Plan = P.Id_Plan
		WHERE Id_User = pIdUser;
    END IF;
    
END$$

DELIMITER ;

DROP procedure IF EXISTS `sp_Plans`;
DELIMITER $$
CREATE DEFINER=`admin`@`%`PROCEDURE `sp_Plans` (
	IN pOpc						char(3),
	IN pIdPlan 					int,
    IN pNamePlan				varchar(30),
	IN pTypePlan	 			varchar(10),
	IN pPricePlan	 			decimal(15,2),
	IN pStatusPlan 				boolean
    
   /* `Id_Plan` int NOT NULL AUTO_INCREMENT,
  `Name_Plan` varchar(30) NOT NULL,
  `Type_Plan` boolean NOT NULL, # 1 mensual 0 anual 
  `Price_Plan` decimal(15,2) NOT NULL,
  `Status_Plan` boolean NULL DEFAULT '1',
  PRIMARY KEY (`Id_Plan`),
  UNIQUE KEY `IdPlan_UNIQUE` (`Id_Plan`) */
)
BEGIN
	IF pOpc = 'I'
	THEN 
		INSERT INTO plans(	Name_Plan, Type_Plan, Price_Plan )
			VALUES(	pNamePlan, pTypePlan, pPricePlan );
	END IF;
	IF pOpc = 'U'
	THEN
		UPDATE plans
			SET Name_Plan = pNamePlan,
				Type_Plan = pTypePlan,
				Price_Plan = pPricePlan
			WHERE Id_Plan = pIdPlan;
            
	END IF;
    
    IF pOpc = 'D'
	THEN 
		DELETE
			FROM plans
            WHERE Id_Plan = pIdPlan;
	END IF;

	IF pOpc = 'LC'
	THEN
		UPDATE plans
			SET Status_Plan = pStatusPlan
		WHERE Id_Plan = pIdPlan;
	END IF;
    
	IF pOpc = 'X'
	THEN
		SELECT Id_Plan, Name_Plan, Type_Plan, Price_Plan, 
				Status_Plan
			FROM plans;
	END IF;
    
    IF pOpc = 'S'
    THEN
		SELECT Id_Plan, Name_Plan, Type_Plan, Price_Plan, 
				Status_Plan
			FROM plans
		WHERE Id_Plan = pIdPlan;
    END IF;
    
    IF pOpc = 'BP' #checa cual es el plan mas contratado por los usuarios siendo siempre mejor que el plan actual, si se tiene el mejor, no aparece nada
    THEN
		
		SELECT U.Id_Plan,  COUNT(U.Id_Plan) Better_Plan,  P.Name_Plan, P.Type_Plan, P.Price_Plan, P.Status_Plan 
			FROM Plans as P
		INNER JOIN Users as U
        ON U.Id_Plan = P.Id_Plan
        WHERE U.Id_Plan > pIdPlan
        AND Status_Plan IS TRUE
		GROUP BY U.Id_Plan
        ORDER BY COUNT(U.Id_Plan) DESC LIMIT 1;
        
    END IF;  
    
     IF pOpc = 'NP' #Este se trae los siguientes 3 planes si es que ya tiene el mas comprado
    THEN
		
		SELECT Id_Plan,  Name_Plan, Type_Plan, Price_Plan, Status_Plan 
			FROM Plans
        WHERE Id_Plan > pIdPlan
        AND Status_Plan IS TRUE
        ORDER BY Id_Plan ASC LIMIT 0, 3;
        
    END IF;  
    
END$$

DELIMITER ;

DROP procedure IF EXISTS `sp_Payments`;
DELIMITER $$
CREATE DEFINER=`admin`@`%`PROCEDURE `sp_Payments` (
	IN pOpc						char(3),
	IN pIdPayment 				bigint,
	IN pIdUser		 			bigint,
	IN pIdPlan	 				int,
    IN pBeginDatePayment		date,
	IN pCutoffDatePayment		date,
    IN pAmountPayment			decimal(15,2),
    IN pStatusPayment			boolean
    
    /* `Id_Payment` bigint NOT NULL AUTO_INCREMENT,
  `Id_User` bigint NOT NULL,
  `Id_Plan` int NOT NULL,
  `BeginDate_Payment` date NOT NULL,
  `CutoffDate_Payment` date NOT NULL,
  `Amount_Payment` decimal(15,2) NOT NULL 
  `Status_Payment` boolean NOT NULL DEFAULT 0,
  */
)
BEGIN
	IF pOpc = 'I'
	THEN 
		INSERT INTO payments(	Id_User, Id_Plan, BeginDate_Payment, CutoffDate_Payment, Amount_Payment, Status_Payment )
			VALUES(	pIdUser, pIdPlan, pBeginDatePayment, pCutoffDatePayment, pAmountPayment, pStatusPayment );

	END IF;
	IF pOpc = 'U'
	THEN
		UPDATE payments
			SET Id_User = pIdUser,
				Id_Plan = pIdPlan,
                BeginDate_Payment = pBeginDatePayment,
                CutoffDate_Payment = pCutoffDatePayment,
                Amount_Payment = pAmountPayment,
                Status_Payment = pStatusPayment
			WHERE Id_Payment = pIdPayment;
            
	END IF;
    
	IF pOpc = 'D'
	THEN 
		DELETE
			FROM payments
            WHERE Id_Payment = pIdPayment;
	END IF;
	
    IF pOpc = 'LC'
	THEN
		UPDATE payments
			SET Status_Payment = pStatusPayment
		WHERE Id_Payment = pIdPayment;
	END IF;
    
	IF pOpc = 'X'
	THEN
		SELECT Id_Payment, Id_User, Id_Plan, BeginDate_Payment, CutoffDate_Payment, Amount_Payment, Status_Payment
			FROM payments;
	END IF;
    
    IF pOpc = 'S'
    THEN
		SELECT Id_Payment, Id_User, Id_Plan, BeginDate_Payment, CutoffDate_Payment, Amount_Payment, Status_Payment
			FROM payments
		WHERE Id_Payment = pIdPayment;
    END IF;
    
	IF pOpc = 'UP'
    THEN
		SELECT Id_Payment, Id_User, py.Id_Plan, p.Name_Plan, p.Type_Plan, BeginDate_Payment, CutoffDate_Payment, Amount_Payment, Status_Payment
			FROM payments as py
		INNER JOIN plans as p
        ON py.Id_Plan = p.Id_Plan
		WHERE Id_User = pIdUser;
    END IF;  
    
    IF pOpc = 'DUP'
    THEN
		SELECT Id_Payment, Id_User, py.Id_Plan, p.Name_Plan, p.Type_Plan, BeginDate_Payment, CutoffDate_Payment, Amount_Payment, Status_Payment
			FROM payments as py
		INNER JOIN plans as p
        ON py.Id_Plan = p.Id_Plan
		WHERE Id_User = pIdUser
        AND BeginDate_Payment >= pBeginDatePayment;
        
    END IF;  
    
    IF pOpc = 'CPY'
    THEN
		SELECT Id_Payment, Id_User, py.Id_Plan, p.Name_Plan, p.Type_Plan, p.Price_Plan, BeginDate_Payment, CutoffDate_Payment, Amount_Payment, Status_Payment
			FROM payments as py
		INNER JOIN plans as p
        ON py.Id_Plan = p.Id_Plan
		WHERE Id_User = pIdUser
        ORDER BY Id_Payment DESC LIMIT 1;
    END IF; 
    
END$$

DELIMITER ;