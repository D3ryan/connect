SET GLOBAL event_scheduler = ON;

DROP EVENT newPayment
DELIMITER $$
CREATE EVENT newPayment
  ON SCHEDULE
    EVERY 1 DAY
    STARTS (TIMESTAMP(CURRENT_DATE) + INTERVAL 1 DAY + INTERVAL 1 HOUR) ON COMPLETION PRESERVE
  DO
  BEGIN
  
	INSERT INTO payments(Id_Payment, Id_User, Id_Plan, BeginDate_Payment, CutoffDate_Payment, Amount_Payment, Status_Payment)
	SELECT Id_Payment, Id_User, Id_Plan, BeginDate_Payment, CutoffDate_Payment, Amount_Payment, Status_Payment
	FROM ActiveUsersFree_view
	WHERE BeginDate_Payment = CURRENT_DATE
	ON DUPLICATE KEY UPDATE Status_Payment = 1;

    INSERT INTO payments( Id_User, Id_Plan, BeginDate_Payment, CutoffDate_Payment, Amount_Payment, Status_Payment)
	SELECT Id_User, Id_Plan, CutoffDate_Payment as BeginDate_Payment, DATE_ADD(CURRENT_DATE(), INTERVAL 1 MONTH) as CutoffDate_Payment , Price_Plan, 1 as Status_Payment
		FROM ActiveUsers_view											 #CURRENT DATE + 1 Month
	WHERE CutoffDate_Payment = CURRENT_DATE 
	AND Type_Plan = 'Mensual'; 
    
	INSERT INTO payments( Id_User, Id_Plan, BeginDate_Payment, CutoffDate_Payment, Amount_Payment, Status_Payment)
	SELECT Id_User, Id_Plan, CutoffDate_Payment as BeginDate_Payment, DATE_ADD(CURRENT_DATE(), INTERVAL 1 YEAR) as CutoffDate_Payment , Price_Plan * 12, 1 as Status_Payment
		FROM ActiveUsers_view											 #CURRENT DATE + 1 Year
	WHERE CutoffDate_Payment = CURRENT_DATE
	AND Type_Plan = 'Anual'; 
	
  END $$

  
  
	