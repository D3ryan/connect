Create database connect_db;
USE connect_db;

/*CREATE TABLES*/
DROP table IF EXISTS `Users`;
CREATE TABLE `Users` (
  `Id_User` bigint NOT NULL AUTO_INCREMENT,
  `Id_Plan` int NOT NULL,
  `FirstName_User` varchar(30) NOT NULL,
  `LastName_User` varchar(30) NOT NULL,
  `Email_User` varchar(60) NOT NULL,
  `Password_User` varchar(60) NOT NULL,
  `LicenseType_User` varchar(10) NOT NULL,
  `Country_User` varchar(80) NOT NULL,
  `CardNumber_User` varchar(30) NOT NULL,
  `Month_User` int NOT NULL,
  `Year_User` int NOT NULL,
  `SecurityNumber_User` varchar(5) NOT NULL,
  `Cardholder_User` varchar(100) NOT NULL,
  `Status_User` boolean NOT NULL DEFAULT '1',
  PRIMARY KEY (`Id_User`),
  UNIQUE KEY `IdUser_UNIQUE` (`Id_User`),
  UNIQUE KEY `EmailUser_UNIQUE` (`Email_User`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP table IF EXISTS `Plans`;
CREATE TABLE `Plans` (
  `Id_Plan` int NOT NULL AUTO_INCREMENT,
  `Name_Plan` varchar(30) NOT NULL,
  `Type_Plan` varchar(10) NOT NULL,
  `Price_Plan` decimal(15,2) NOT NULL,
  `Status_Plan` boolean NULL DEFAULT '1',
  PRIMARY KEY (`Id_Plan`),
  UNIQUE KEY `IdPlan_UNIQUE` (`Id_Plan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
 
DROP table IF EXISTS `Payments`;
CREATE TABLE `Payments` (
  `Id_Payment` bigint NOT NULL AUTO_INCREMENT,
  `Id_User` bigint NOT NULL,
  `Id_Plan` int NOT NULL,
  `BeginDate_Payment` date NOT NULL,
  `CutoffDate_Payment` date NOT NULL,
  `Amount_Payment` decimal(15,2) NOT NULL,
  `Status_Payment` boolean NOT NULL DEFAULT 0,
  PRIMARY KEY (`Id_Payment`),
  UNIQUE KEY `IdPayment_UNIQUE` (`Id_Payment`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


/*ALTER TABLES*/

ALTER TABLE `Payments` 
ADD INDEX `FK_ID_USER_INDX` (`Id_User` ASC)  VISIBLE,
ADD INDEX `FK_ID_PLAN_INDX` (`Id_Plan` ASC)  VISIBLE;

ALTER TABLE `Payments`
ADD CONSTRAINT `FK_ID_USER`
  FOREIGN KEY (`Id_User`)
  REFERENCES `users` (`Id_User`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `FK_ID_PLAN`
  FOREIGN KEY (`Id_Plan`)
  REFERENCES `plans` (`Id_Plan`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
  
ALTER TABLE `users` 
ADD INDEX `FK_ID_PLAN_INDX` (`Id_Plan` ASC)  VISIBLE;

ALTER TABLE `users`
ADD CONSTRAINT `FK_ID_PLAN_USER`
  FOREIGN KEY (`Id_Plan`)
  REFERENCES `plans` (`Id_Plan`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

#DROPS 

ALTER TABLE `users`
DROP FOREIGN KEY `FK_ID_PLAN_USER`;

ALTER TABLE `users`
DROP INDEX `FK_ID_PLAN`;

ALTER TABLE `payments`
DROP FOREIGN KEY `FK_ID_USER`;

ALTER TABLE `payments`
DROP FOREIGN KEY `FK_ID_PLAN`;

TRUNCATE TABLE `users`;
TRUNCATE TABLE `plans`;
TRUNCATE TABLE `payments`;

#Changes
ALTER TABLE `Users` CHANGE COLUMN `LicensceType_User` `LicenseType_User`  varchar(10) NOT NULL;
ALTER TABLE `Plans` CHANGE COLUMN `Type_Plan` `Type_Plan`  varchar(10) NOT NULL;
ALTER TABLE `Payments` ADD COLUMN  `BeginDate_Payment` date NOT NULL;
ALTER TABLE `Payments` ADD COLUMN  `StatusPayment` boolean NOT NULL DEFAULT 1;
ALTER TABLE `Payments` CHANGE COLUMN  `StatusPayment` `Status_Payment` boolean NOT NULL DEFAULT 0;

#Calls de procedures

CALL sp_Plans('X', NULL,  NULL, NULL, NULL, NULL);
CALL sp_Plans('U', 1,  'Básico', 'Mensual', 26.99, 1);
CALL sp_Plans('I', NULL,  'Básico', 'Mensual', 26.99, NULL);
CALL sp_Plans('I', NULL,  'Profesional', 'Mensual', 64.99, NULL);
CALL sp_Plans('I', NULL,  'Empresarial', 'Mensual', 94.99, NULL);
CALL sp_Plans('I', NULL,  'Básico', 'Anual', 22.99, NULL);
CALL sp_Plans('I', NULL,  'Profesional', 'Anual', 59.99, NULL);
CALL sp_Plans('I', NULL,  'Empresarial', 'Anual', 89.99, NULL);

CALL sp_Plans('I', NULL,  'Ultimate', 'De por vida', 89.99, NULL); #checar luego

CALL sp_Plans('D', 1, NULL, NULL, NULL, NULL);

CALL sp_Users('I',  NULL, 1, 'Mario', 'Galvan', 'a', '121212', 'Compania', 'Mexico', '12312312312', 1, 2020, '1213', 'Mario Galvan', NULL);
CALL sp_Users('X', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
CALL sp_Users('D', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
CALL sp_Users('LC', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);

CALL sp_Payments('I', NULL, 2, 4, '2023-05-25', '2024-05-25', 26.99, true);
CALL sp_Payments('X', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
CALL sp_Payments('U', 1, 1, 1, '2021-05-25', '2021-06-25', 26.99, 0);
CALL sp_Payments('U', 2, 1, 4, '2021-05-27', '2021-05-27', 275.88, 1);

#Views
#view con los usuarios regulares activos
CREATE VIEW ActiveUsers_view AS
	SELECT Id_Payment, U.Id_User, U.Id_Plan, P.Type_Plan, P.Price_Plan, BeginDate_Payment, CutoffDate_Payment, Amount_Payment, Status_Payment
		FROM users as U
        INNER JOIN plans as P
        ON U.Id_Plan = P.Id_Plan
        INNER JOIN payments as PY
        ON U.Id_User = PY.Id_User
    WHERE Status_User = 1
    AND Status_Payment = 1
    AND Id_Payment IN (SELECT MAX(Id_Payment) FROM payments GROUP BY Id_User); 
    
#view con los usuarios con prueba es de apoyo
CREATE VIEW ActiveUsersFree_view AS 
	SELECT Id_Payment, U.Id_User, U.Id_Plan, P.Price_Plan, BeginDate_Payment, CutoffDate_Payment, Amount_Payment, Status_Payment
		FROM users as U
        INNER JOIN plans as P
        ON U.Id_Plan = P.Id_Plan
        INNER JOIN payments as PY
        ON U.Id_User = PY.Id_User
    WHERE Status_User = 1
    AND Status_Payment = 0
    AND Id_Payment IN (SELECT MAX(Id_Payment) FROM payments GROUP BY Id_User); 

SELECT * FROM ActiveUsers_view;
SELECT * FROM ActiveUsersFree_view;

#QUERY's
#Query para actualizar a los usuarios con prueba

INSERT INTO payments(Id_Payment, Id_User, Id_Plan, BeginDate_Payment, CutoffDate_Payment, Amount_Payment, Status_Payment)
SELECT Id_Payment, Id_User, Id_Plan, BeginDate_Payment, CutoffDate_Payment, Amount_Payment, Status_Payment
	FROM ActiveUsersFree_view
WHERE BeginDate_Payment = '2021-06-11' #current date
ON DUPLICATE KEY UPDATE Status_Payment = 1;

#Query para insertar nuevos pagos 
# Mensuales
INSERT INTO payments( Id_User, Id_Plan, BeginDate_Payment, CutoffDate_Payment, Amount_Payment, Status_Payment)
SELECT Id_User, Id_Plan, CutoffDate_Payment as BeginDate_Payment, DATE_ADD('2021-07-11', INTERVAL 1 MONTH) as CutoffDate_Payment , Price_Plan, 1 as Status_Payment
	FROM ActiveUsers_view											 #CURRENT DATE
WHERE CutoffDate_Payment = '2021-07-11'
AND Type_Plan = 'Mensual'
AND Status_Payment = 1; #CURRENT DATE

#Query para insertar nuevos pagos 
# Anuales
INSERT INTO payments( Id_User, Id_Plan, BeginDate_Payment, CutoffDate_Payment, Amount_Payment, Status_Payment)
SELECT Id_User, Id_Plan, CutoffDate_Payment as BeginDate_Payment, DATE_ADD('2022-05-27', INTERVAL 1 YEAR) as CutoffDate_Payment , Price_Plan * 12, 1 as Status_Payment
	FROM ActiveUsers_view											 #CURRENT DATE
WHERE CutoffDate_Payment = '2022-05-27'
AND Type_Plan = 'Anual'; #CURRENT DATE


/* QUERYS ANTIGUOS 
#Query Manual para cobro
INSERT INTO payments( Id_User, Id_Plan, BeginDate_Payment, CutoffDate_Payment, Amount_Payment, Status_Payment)
SELECT Id_User, Id_Plan, CutoffDate_Payment as BeginDate_Payment, '2024-07-09' as CutoffDate_Payment, Amount_Payment, 1 as Status_Payment
	FROM ActiveUsers_view
WHERE Id_User = 2
AND CutoffDate_Payment = '2023-05-25'
ORDER BY Id_Payment DESC LIMIT 1;

#query de apoyo
SELECT Id_Payment, Id_User, py.Id_Plan, p.Name_Plan, p.Type_Plan, BeginDate_Payment, CutoffDate_Payment, Amount_Payment, Status_Payment
	FROM payments as py
INNER JOIN plans as p
ON py.Id_Plan = p.Id_Plan
WHERE Id_User = 7
ORDER BY Id_Payment DESC LIMIT 1; 

SET SQL_SAFE_UPDATES = 0;

UPDATE payments as PY
INNER JOIN users as U
ON PY.Id_User = U.Id_User
	SET PY.Status_Payment = 1
WHERE PY.BeginDate_Payment = '2021-05-25' #current date
AND  PY.Status_Payment = 0
AND U.Status_User = 1;

SET SQL_SAFE_UPDATES = 1;
*/
        