<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.Connect.models.*"%>
<%@page import="java.util.*"%>
    
<%

List<Plans_Model> PlansList = (List<Plans_Model>) request.getAttribute("PlanList");
pageContext.setAttribute("PlanList", PlansList);

Users_Model selectedUser = (Users_Model)request.getAttribute("SelectedUser");
pageContext.setAttribute("SelectedUser", selectedUser);

if(request.getAttribute("plan") != null){
	int plan = (int) request.getAttribute("plan");
	pageContext.setAttribute("plan", plan);
}

%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/bootstrap.min.css" 		type="text/css" rel="stylesheet">
    <link href="css/general.css"         	type="text/css" rel="stylesheet">
    <link href="css/navbar.css"         	type="text/css" rel="stylesheet">
    <link href="css/dash-account.css"    	type="text/css" rel="stylesheet">
    <link href="css/validation.css"        type="text/css" rel="stylesheet">
    <link href="css/introjs.css"         	  type="text/css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <script src="https://kit.fontawesome.com/ea1f7a49e6.js" crossorigin="anonymous"></script>
    <script src="js/jquery-3.6.0.min.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <script src="js/additional-methods.min.js"></script>
    <script src="js/generalValidation.js"></script>
    <script src="js/account.js"></script>
    <script src="js/intro.js"></script>
    <title>Connect - Josue Animations</title>
</head>
<body>
    <header>
    	<jsp:include page="navbar.jsp"></jsp:include>
    </header>
    <main>
        <div class="container-fluid">
           <div class="row flex-column flex-sm-row min-vh-100">
                <div class="df_dashnav col-12 col-sm-2 p-0">
                	<jsp:include page="dash-navbar.jsp"></jsp:include>
                </div>
               <div class="col-12 col-sm-10">
                <c:if test="${ SelectedUser.isStatusUser() == true }">
                    <div class="row justify-content-center">
                        <form action="DashAccount" method="post" id="updateForm" class="df_account-container col-sm-8 col-lg-6 col-xl-5 col-xxl-4 p-4"
                         data-step="1" data-title="Actualiza tu cuenta" data-intro="Aquí puedes actualizar tu información general y la de facturación" data-position="left">
                            <div class="df_user-info">
                                <p class="df_text-h1 df_text text-center">Actualiza tu cuenta</p>
                                <div class="form-floating mb-3">
                                    <input type="text" name="name" class="df_input form-control" id="floatingInput" placeholder="Nombre(s)" value="${ SelectedUser.getFirstNameUser()}">
                                    <label for="floatingInput" class="df_label">Nombre(s)</label>
                                </div>
                                <div class="form-floating mb-3">
                                    <input type="text" name="lastname" class="df_input form-control" id="floatingInput" placeholder="Apellido(s)" value="${ SelectedUser.getLastNameUser()}">
                                    <label for="floatingInput" class="df_label">Apellido(s)</label>
                                </div>
                                <div class="form-floating mb-3">
                                    <input type="text" name="password" class="df_input form-control" id="floatingPassword" placeholder="Password" value="${ SelectedUser.getPasswordUser()}">
                                    <label for="floatingPassword" class="df_label">Contraseña</label>
                                </div>
                            </div>
                            <div class="df_billing-info w-100">
                                <p class="df_text text-white">Información de facturación</p>
                                <div class="select-container">
                                    <select name="plantype" class="df_text df_input df_select form-select mb-3" aria-label="select">
                                    	<c:if test="${plan == null}">
	                                    	<option value="">Tipo de plan</option>
	                                    	<c:forEach var="iPlan" items="${PlanList}">
												<c:if test="${ SelectedUser.getId_Plan() == iPlan.getId_Plan() }">
													<option selected value="${ iPlan.getId_Plan() }">${ iPlan.getNamePlan() } ${ iPlan.getTypePlan() } $${ iPlan.getPricePlan() } USD/m</option>
												</c:if>
												<c:if test="${ SelectedUser.getId_Plan() != iPlan.getId_Plan() }">
													<option value="${ iPlan.getId_Plan() }">${ iPlan.getNamePlan() } ${ iPlan.getTypePlan() } $${ iPlan.getPricePlan() } USD/m</option>
												</c:if>
													
											</c:forEach>
										</c:if>
										
										<c:if test="${plan != null}">
											<option value="">Tipo de plan</option>
	                                     	<c:forEach var="iPlan" items="${PlanList}">
	                                     		<c:if test="${ plan == iPlan.getId_Plan() }">
	                                     			<option selected value="${ iPlan.getId_Plan() }">${ iPlan.getNamePlan() } ${ iPlan.getTypePlan() } $${ iPlan.getPricePlan() } USD/m</option>
	                                     		</c:if>
	                                     			<option value="${ iPlan.getId_Plan() }">${ iPlan.getNamePlan() } ${ iPlan.getTypePlan() } $${ iPlan.getPricePlan() } USD/m</option>
	                                     	</c:forEach>
										</c:if>
                                    </select>
                                </div>
                                <div class="select-container">
                                    <select name="country" class="df_text df_input df_select form-select mb-3" aria-label="select">
                                        <option value="">																					   País</option>
                                        <option value="México" 		   ${ SelectedUser.getCountryUser() == 'México' ? 'selected' : ''}>        México</option>
                                        <option value="Estados Unidos" ${ SelectedUser.getCountryUser() == 'Estados Unidos' ? 'selected' : ''}>Estados Unidos</option>
                                        <option value="Canadá" 	  	   ${ SelectedUser.getCountryUser() == 'Canadá' ? 'selected' : ''}>		   Canadá</option>
                                        <option value="Honduras"  	   ${ SelectedUser.getCountryUser() == 'Honduras' ? 'selected' : ''}>      Honduras</option>
                                        <option value="Panamá" 	  	   ${ SelectedUser.getCountryUser() == 'Panamá' ? 'selected' : ''}>        Panamá</option>
                                        <option value="Colombia"  	   ${ SelectedUser.getCountryUser() == 'Colombia' ? 'selected' : ''}>      Colombia</option>
                                        <option value="Argentina" 	   ${ SelectedUser.getCountryUser() == 'Argentina' ? 'selected' : ''}>     Argentina</option>
                                        <option value="Brasil" 	  	   ${ SelectedUser.getCountryUser() == 'Brasil' ? 'selected' : ''}>        Brasil</option> 
                                        <option value="Ecuador"   	   ${ SelectedUser.getCountryUser() == 'Ecuador' ? 'selected' : ''}>       Ecuador</option> 
                                        <option value="Chile"     	   ${ SelectedUser.getCountryUser() == 'Chile' ? 'selected' : ''}>         Chile</option>
                                        <option value="El Salvador"    ${ SelectedUser.getCountryUser() == 'El Salvador' ? 'selected' : ''}>   El Salvador</option> 
                                        <option value="Belice" 		   ${ SelectedUser.getCountryUser() == 'Belice' ? 'selected' : ''}>        Belice</option> 
                                    </select>
                                </div>
                                <div class="form-floating mb-3">
                                    <input name="cardnumber" type="number" class="df_input df_input-number form-control" id="floatingCard" placeholder="Number" value="${ SelectedUser.getCardNumberUser() }">
                                    <label for="floatingCard" class="df_label">Número de tarjeta</label>
                                </div>
                                <p class="df_text text-white">Fecha de vencimiento:</p>
                                <div class="row mb-3">
                                    <div class="col-6">
                                        <select name="month" class="df_input df_select form-select col-6" aria-label="Default select example">
                                            
                                            <option value="">Mes</option>
                                            <option value="1" ${ SelectedUser.getMonthUser() == 1 ? 'selected' : ''}>01</option>
                                            <option value="2" ${ SelectedUser.getMonthUser() == 2 ? 'selected' : ''}>02</option>
                                            <option value="3" ${ SelectedUser.getMonthUser() == 3 ? 'selected' : ''}>03</option>
                                            <option value="4" ${ SelectedUser.getMonthUser() == 4 ? 'selected' : ''}>04</option>
                                            <option value="5" ${ SelectedUser.getMonthUser() == 5 ? 'selected' : ''}>05</option>
                                            <option value="6" ${ SelectedUser.getMonthUser() == 6 ? 'selected' : ''}>06</option>
                                            <option value="7" ${ SelectedUser.getMonthUser() == 7 ? 'selected' : ''}>07</option>
                                            <option value="8" ${ SelectedUser.getMonthUser() == 8 ? 'selected' : ''}>08</option>
                                            <option value="9" ${ SelectedUser.getMonthUser() == 9 ? 'selected' : ''}>09</option>
                                            <option value="10" ${ SelectedUser.getMonthUser() == 10 ? 'selected' : ''}>10</option>
                                            <option value="11" ${ SelectedUser.getMonthUser() == 11 ? 'selected' : ''}>11</option>
                                            <option value="12" ${ SelectedUser.getMonthUser() == 12 ? 'selected' : ''}>12</option>
                                        </select>
                                    </div>
                                    <div class="col-6">
                                        <select name="year" class="df_input df_select form-select col-6" aria-label="Default select example">
                                            <option value="">Año</option>
                                            <option value="2021" ${ SelectedUser.getYearUser() == 2021 ? 'selected' : ''}>2021</option>
                                            <option value="2022" ${ SelectedUser.getYearUser() == 2022 ? 'selected' : ''}>2022</option>
                                            <option value="2023" ${ SelectedUser.getYearUser() == 2023 ? 'selected' : ''}>2023</option>
                                            <option value="2024" ${ SelectedUser.getYearUser() == 2024 ? 'selected' : ''}>2024</option>
                                            <option value="2025" ${ SelectedUser.getYearUser() == 2025 ? 'selected' : ''}>2025</option>
                                            <option value="2026" ${ SelectedUser.getYearUser() == 2026 ? 'selected' : ''}>2026</option>
                                            <option value="2027" ${ SelectedUser.getYearUser() == 2027 ? 'selected' : ''}>2027</option>
                                            <option value="2028" ${ SelectedUser.getYearUser() == 2028 ? 'selected' : ''}>2028</option>
                                            <option value="2029" ${ SelectedUser.getYearUser() == 2029 ? 'selected' : ''}>2029</option>
                                            <option value="2030" ${ SelectedUser.getYearUser() == 2030 ? 'selected' : ''}>2030</option>
                                            <option value="2031" ${ SelectedUser.getYearUser() == 2031 ? 'selected' : ''}>2031</option>
                                            <option value="2032" ${ SelectedUser.getYearUser() == 2032 ? 'selected' : ''}>2032</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-floating mb-3">
                                    <input name="securitycode"  type="number" class="df_input df_input-number form-control" id="floatingCode" placeholder="Code" value="${ SelectedUser.getSecurityNumberUser() }">
                                    <label for="floatingCode" class="df_label">Código de seguridad</label>
                                </div>
                                <div class="form-floating mb-3">
                                    <input name="cardholder" type="text" class="df_input form-control" id="floatingOwner" placeholder="Owner" value="${ SelectedUser.getCardholderUser() }">
                                    <label for="floatingOwner" class="df_label">Titular</label>
                                </div>
                                <div class="d-grid col-6 mb-4 mx-auto">
                                    <button type="button" id="btn_update" form="updateForm" class="df_btn me-2">Actualizar</button>
                                </div>
                            </div>
                        </form>   	
                    </div>
                    <div class="row justify-content-center">
                        <div class="df_account-container mt-0 col-sm-8 col-lg-6 col-xl-5 col-xxl-4 p-4">
                            <div class="df_deactivate d-flex flex-column justify-content-center align-items-center">
                                <p class="df_text text-center">¿Deseas finalizar el contrato de tu plan?</p>
                                <button type="button" class="df_btn df_btn-red" data-bs-toggle="modal" data-bs-target="#exampleModal" 
                                data-step="2" data-title="Finaliza tu plan" data-intro="Si deseas finalizar tu plan puedes hacerlo con este botón." data-position="left">
                                  	Finalizar
                                </button>
                            </div>                
                        </div>
                    </div>
                </c:if>
                    
                <c:if test="${ SelectedUser.isStatusUser() == false }">
                    <div class="row justify-content-center">
                        <form action="DashAccount" method="post" id="activateForm" class="df_account-container col-sm-8 col-lg-6 col-xl-5 col-xxl-4 p-4" 
                        data-step="1" data-title="Reactiva tu cuenta" data-intro="Aquí puedes actualizar tu información y reactivar tu cuenta." data-position="left">
                            <div class="df_user-info">
                                <p class="df_text-h1 df_text text-center">Reactiva tu cuenta</p>
                                <div class="form-floating mb-3">
                                    <input type="text" name="name" class="df_input form-control" id="floatingInput" placeholder="Nombre(s)" value="${ SelectedUser.getFirstNameUser()}">
                                    <label for="floatingInput" class="df_label">Nombre(s)</label>
                                </div>
                                <div class="form-floating mb-3">
                                    <input type="text" name="lastname" class="df_input form-control" id="floatingInput" placeholder="Apellido(s)" value="${ SelectedUser.getLastNameUser()}">
                                    <label for="floatingInput" class="df_label">Apellido(s)</label>
                                </div>
                                <div class="form-floating mb-3">
                                    <input type="text" name="password" class="df_input form-control" id="floatingPassword" placeholder="Password" value="${ SelectedUser.getPasswordUser()}">
                                    <label for="floatingPassword" class="df_label">Contraseña</label>
                                </div>
                            </div>
                            <div class="df_billing-info w-100">
                                <p class="df_text text-white">Información de facturación</p>
                                <div class="select-container">
                                    <select name="plantype" class="df_text df_input df_select form-select mb-3" aria-label="select">
                                    	<c:if test="${plan == null}">
	                                    	<option value="">Tipo de plan</option>
	                                    	<c:forEach var="iPlan" items="${PlanList}">
												<c:if test="${ SelectedUser.getId_Plan() == iPlan.getId_Plan() }">
													<option selected value="${ iPlan.getId_Plan() }">${ iPlan.getNamePlan() } ${ iPlan.getTypePlan() } $${ iPlan.getPricePlan() } USD/m</option>
												</c:if>
												<c:if test="${ SelectedUser.getId_Plan() != iPlan.getId_Plan() }">
													<option value="${ iPlan.getId_Plan() }">${ iPlan.getNamePlan() } ${ iPlan.getTypePlan() } $${ iPlan.getPricePlan() } USD/m</option>
												</c:if>
													
											</c:forEach>
										</c:if>
										
										<c:if test="${plan != null}">
											<option value="">Tipo de plan</option>
	                                     	<c:forEach var="iPlan" items="${PlanList}">
	                                     		<c:if test="${ plan == iPlan.getId_Plan() }">
	                                     			<option selected value="${ iPlan.getId_Plan() }">${ iPlan.getNamePlan() } ${ iPlan.getTypePlan() } $${ iPlan.getPricePlan() } USD/m</option>
	                                     		</c:if>
	                                     			<option value="${ iPlan.getId_Plan() }">${ iPlan.getNamePlan() } ${ iPlan.getTypePlan() } $${ iPlan.getPricePlan() } USD/m</option>
	                                     	</c:forEach>
										</c:if>
                                    </select>
                                </div>
                                <div class="select-container">
                                    <select name="country" class="df_text df_input df_select form-select mb-3" aria-label="select">
                                        <option value="">																					   País</option>
                                        <option value="México" 		   ${ SelectedUser.getCountryUser() == 'México' ? 'selected' : ''}>        México</option>
                                        <option value="Estados Unidos" ${ SelectedUser.getCountryUser() == 'Estados Unidos' ? 'selected' : ''}>Estados Unidos</option>
                                        <option value="Canadá" 	  	   ${ SelectedUser.getCountryUser() == 'Canadá' ? 'selected' : ''}>		   Canadá</option>
                                        <option value="Honduras"  	   ${ SelectedUser.getCountryUser() == 'Honduras' ? 'selected' : ''}>      Honduras</option>
                                        <option value="Panamá" 	  	   ${ SelectedUser.getCountryUser() == 'Panamá' ? 'selected' : ''}>        Panamá</option>
                                        <option value="Colombia"  	   ${ SelectedUser.getCountryUser() == 'Colombia' ? 'selected' : ''}>      Colombia</option>
                                        <option value="Argentina" 	   ${ SelectedUser.getCountryUser() == 'Argentina' ? 'selected' : ''}>     Argentina</option>
                                        <option value="Brasil" 	  	   ${ SelectedUser.getCountryUser() == 'Brasil' ? 'selected' : ''}>        Brasil</option> 
                                        <option value="Ecuador"   	   ${ SelectedUser.getCountryUser() == 'Ecuador' ? 'selected' : ''}>       Ecuador</option> 
                                        <option value="Chile"     	   ${ SelectedUser.getCountryUser() == 'Chile' ? 'selected' : ''}>         Chile</option>
                                        <option value="El Salvador"    ${ SelectedUser.getCountryUser() == 'El Salvador' ? 'selected' : ''}>   El Salvador</option> 
                                        <option value="Belice" 		   ${ SelectedUser.getCountryUser() == 'Belice' ? 'selected' : ''}>        Belice</option> 
                                    </select>
                                </div>
                                <div class="form-floating mb-3">
                                    <input name="cardnumber" type="number" class="df_input df_input-number form-control" id="floatingCard" placeholder="Number" value="${ SelectedUser.getCardNumberUser() }">
                                    <label for="floatingCard" class="df_label">Número de tarjeta</label>
                                </div>
                                <p class="df_text text-white">Fecha de vencimiento:</p>
                                <div class="row mb-3">
                                    <div class="col-6">
                                        <select name="month" class="df_input df_select form-select col-6" aria-label="Default select example">
                                            
                                            <option value="">Mes</option>
                                            <option value="1" ${ SelectedUser.getMonthUser() == 1 ? 'selected' : ''}>01</option>
                                            <option value="2" ${ SelectedUser.getMonthUser() == 2 ? 'selected' : ''}>02</option>
                                            <option value="3" ${ SelectedUser.getMonthUser() == 3 ? 'selected' : ''}>03</option>
                                            <option value="4" ${ SelectedUser.getMonthUser() == 4 ? 'selected' : ''}>04</option>
                                            <option value="5" ${ SelectedUser.getMonthUser() == 5 ? 'selected' : ''}>05</option>
                                            <option value="6" ${ SelectedUser.getMonthUser() == 6 ? 'selected' : ''}>06</option>
                                            <option value="7" ${ SelectedUser.getMonthUser() == 7 ? 'selected' : ''}>07</option>
                                            <option value="8" ${ SelectedUser.getMonthUser() == 8 ? 'selected' : ''}>08</option>
                                            <option value="9" ${ SelectedUser.getMonthUser() == 9 ? 'selected' : ''}>09</option>
                                            <option value="10" ${ SelectedUser.getMonthUser() == 10 ? 'selected' : ''}>10</option>
                                            <option value="11" ${ SelectedUser.getMonthUser() == 11 ? 'selected' : ''}>11</option>
                                            <option value="12" ${ SelectedUser.getMonthUser() == 12 ? 'selected' : ''}>12</option>
                                        </select>
                                    </div>
                                    <div class="col-6">
                                        <select name="year" class="df_input df_select form-select col-6" aria-label="Default select example">
                                            <option value="">Año</option>
                                            <option value="2021" ${ SelectedUser.getYearUser() == 2021 ? 'selected' : ''}>2021</option>
                                            <option value="2022" ${ SelectedUser.getYearUser() == 2022 ? 'selected' : ''}>2022</option>
                                            <option value="2023" ${ SelectedUser.getYearUser() == 2023 ? 'selected' : ''}>2023</option>
                                            <option value="2024" ${ SelectedUser.getYearUser() == 2024 ? 'selected' : ''}>2024</option>
                                            <option value="2025" ${ SelectedUser.getYearUser() == 2025 ? 'selected' : ''}>2025</option>
                                            <option value="2026" ${ SelectedUser.getYearUser() == 2026 ? 'selected' : ''}>2026</option>
                                            <option value="2027" ${ SelectedUser.getYearUser() == 2027 ? 'selected' : ''}>2027</option>
                                            <option value="2028" ${ SelectedUser.getYearUser() == 2028 ? 'selected' : ''}>2028</option>
                                            <option value="2029" ${ SelectedUser.getYearUser() == 2029 ? 'selected' : ''}>2029</option>
                                            <option value="2030" ${ SelectedUser.getYearUser() == 2030 ? 'selected' : ''}>2030</option>
                                            <option value="2031" ${ SelectedUser.getYearUser() == 2031 ? 'selected' : ''}>2031</option>
                                            <option value="2032" ${ SelectedUser.getYearUser() == 2032 ? 'selected' : ''}>2032</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-floating mb-3">
                                    <input name="securitycode"  type="number" class="df_input df_input-number form-control" id="floatingCode" placeholder="Code" value="${ SelectedUser.getSecurityNumberUser() }">
                                    <label for="floatingCode" class="df_label">Código de seguridad</label>
                                </div>
                                <div class="form-floating mb-3">
                                    <input name="cardholder" type="text" class="df_input form-control" id="floatingOwner" placeholder="Owner" value="${ SelectedUser.getCardholderUser() }">
                                    <label for="floatingOwner" class="df_label">Titular</label>
                                </div>
                                <div class="d-grid col-6 mb-4 mx-auto">
                                    <input type="hidden" name="logicChange" value="true">
                                    <button type="submit" id="btn_activate" class="df_btn me-2" form="activateForm">Activar</button>
                                </div>
                            </div>
                        </form>   	
                    </div>
                </c:if>
               </div>
            </div>
        </div>
    </main>
    <section>
	    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog modal-dialog-centered">
		    <div class="modal-content">
		      <div class="modal-header bg-danger text-white">
		        <h5 class="modal-title" id="exampleModalLabel">¡Cuidado!</h5>
		        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
		      </div>
		      <div class="modal-body">
		      	¿Estás seguro de que quieres finalizar tu plan? Recuerda que NO habrá reembolso.
		      </div>
		      <div class="modal-footer">
		      	<form action="DashAccount" method="post" id="deactivateForm">
		      		<input type="hidden" name="logicChange" value="false">
		      		<button type="submit" class="btn btn-danger" form="deactivateForm">Estoy seguro</button>
		      	</form>
		        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">No</button>
		      </div>
		    </div>
		  </div>
		</div>
		
		<div class="modal fade" id="exampleModal1" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog modal-dialog-centered">
		    <div class="modal-content">
		      <div class="modal-header bg-warning">
		        <h5 class="modal-title" id="exampleModalLabel">¿Estás seguro?</h5>
		        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
		      </div>
		      <div class="modal-body">
		      	Estás apunto de actualizar tu cuenta, si has cambiado de plan se te cobrará el precio del nuevo plan, ¿deseas seguir?
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
		      	<button type="submit" id="btn_updateSecure" class="btn btn-primary" form="updateForm">Estoy seguro</button>
		      </div>
		    </div>
		  </div>
		</div>
		
    </section>
    <footer>
        
    </footer>
    <script src="js/bootstrap.bundle.min.js"></script>
</body>
</html>
