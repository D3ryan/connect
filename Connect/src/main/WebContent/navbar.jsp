<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.Connect.controllers.Navbar"%>
<%@page import="com.Connect.models.*"%>
<%@page import="java.util.*"%>

<%
	Users_Model loggedUser = Navbar.getUser(request, response);
	pageContext.setAttribute("loggedUser", loggedUser);
%>    
<nav class="df_navbar navbar navbar-expand-sm navbar-dark bg-dark">
   <div class="container-fluid">
       <a class="df_brand navbar-brand" href="index.jsp"><img class="df_logo d-inline-block align-top" src="img/Iso Connect.svg" alt="Logo Connect">CONNECT</a>
       <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
           <span class="navbar-toggler-icon"></span>
       </button>
       <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
           <div class="df_navlinks navbar-nav col-6">
               <a class="nav-link" aria-current="page" href="index.jsp">Inicio</a>
               <a class="nav-link" href="Plans">Planes</a>
               <a class="nav-link" href="contact.jsp">Contacto</a>
           </div>
           <div class="navbar-nav justify-content-end col-sm-6">
               <!-- <form action="" class=""> -->
               <c:if test="${empty loggedUser}">
               		<a class="df_btn df_link-btn me-2 text-center" href="login.jsp">Iniciar sesión</a>
               </c:if>
               
               <c:if test="${not empty loggedUser}">
               		<a class="nav-link df_text" href="DashGeneral"><i class="fas fa-user me-2"></i>Hola ¡<span>${ loggedUser.getFirstNameUser() }</span>!</a>
               		<a class="nav-link df_text" href="Login" class="signout-icon_top-nav">
						<i class="fas fa-door-open me-2"></i>
						<span class="d-sm-none d-inline-block">Cerrar Sesión</span> 
					</a>
               </c:if>
               <!-- </form> -->
           </div>
       </div>
   </div>
</nav>