<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<div class="container-fluid">
     <div class="row row-cols-1 row-md-cols-3 p-4">
          <div class="col-12 col-md-4 d-flex align-items-center">
              <a class="df_brand df_link df_brand-footer" href="index.jsp">
                <img class="df_logo-footer d-inline-block align-top" src="img/Iso Connect.svg" alt="Logo Connect">CONNECT
              </a>
          </div>
          <div class="col-12 col-md-4">
            <div class="d-flex flex-column">
              <p class="df_text-footer df_contact">Contacto</p>
              <a class="df_link" href="mailto:recursosvideos510@gmail.com?Subject=Contacto">
                <i class="far fa-envelope"></i> 
                <p class="d-inline-block"> rcsvideos@gmail.com</p>
              </a>
              <div class="df_text-footer">
                <i class="fas fa-phone"></i>
                <p class="d-inline-block"> +52 8991145752</p>
              </div>
            </div>
          </div>
          <div class="col-12 col-md-4">
            <div class="d-flex flex-column">
              <p class="df_text-footer df_contact">Redes Sociales</p>
              <div class="df_text-footer">
                <a href="https://www.instagram.com/josueanimations/" target="_blank" rel="noopener noreferrer" class="df_link df_instagram-containter">
                  <i class="fab fa-instagram"></i>
                  <p class="d-inline-block">@josueanimations</p>
                </a>
              </div>
              <div class="df_text-footer">
                <a href="https://www.facebook.com/josueanimationsoficial" target="_blank" rel="noopener noreferrer" class="df_link df_facebook-container">
                  <i class="fab fa-facebook-square"></i>
                  <p class="d-inline-block">josueanimationsoficial</p>
                </a>
              </div>
              <div class="df_text-footer">
                <a href="https://m.me/josueanimationsoficial" target="_blank" rel="noopener noreferrer" class="df_link df_messenger-container">
                  <i class="fab fa-facebook-messenger"></i>
                  <p class="d-inline-block">josueanimationsoficial</p>
                </a>
              </div>
            </div>
          </div>
     </div>
</div>