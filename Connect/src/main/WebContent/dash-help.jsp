<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/bootstrap.min.css" 	 type="text/css" rel="stylesheet">
    <link href="css/general.css"         type="text/css" rel="stylesheet">
    <link href="css/navbar.css"          type="text/css" rel="stylesheet">
    <link href="css/validation.css"        type="text/css" rel="stylesheet">
    <link href="css/introjs.css"         	  type="text/css" rel="stylesheet">
    <link href="css/jquery.sweet-modal.min.css" type="text/css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <script src="https://kit.fontawesome.com/ea1f7a49e6.js" crossorigin="anonymous"></script>
    <script src="js/jquery-3.6.0.min.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <script src="js/additional-methods.min.js"></script>
    <script src="js/jquery.sweet-modal.min.js"></script>
    <script src="js/generalValidation.js"></script>
    <script src="js/intro.js"></script>
    <title>Connect - Josue Animations</title>
</head>
<body>
    <header>
        <jsp:include page="navbar.jsp"></jsp:include>
    </header>
    <main>
       <div class="container-fluid">
           <div class="row flex-column flex-sm-row min-vh-100">
               <div class="df_dashnav col-12 col-sm-2 p-0">
               		<jsp:include page="dash-navbar.jsp"></jsp:include>
               </div>
               <div class="col-12 col-sm-10">
                    <div class="row justify-content-center">
                        <div class="col-11">
                            <div class="row">
                                <p class="df_text-h1 df_text m-0">Soporte</p>
                                <p class="df_text text-white-50">¿Tienes un problema? ¡Manda tu mensaje nosotros te ayudamos!</p>
                            </div>
                            <div class="row">
                                <form action="DashHelp" method="post" id="supportForm" class="col-12">
                                    <div class="form-floating mb-3">
                                        <input type="text" class="df_input form-control" id="floatingInput" placeholder="Nombre Completo" name="subject" 
                                        data-step="1" data-title="Asunto" data-intro="Aquí puedes escribir tu asunto sobre algún problema o duda que tengas.">
                                        <label for="floatingInput" class="df_label">Asunto</label>
                                    </div>
                                    <div class="form-floating mb-3">
                                        <textarea class="df_input df_textarea form-control" placeholder="Comentario" id="floatingTextarea" name="comment"
                                        data-step="2" data-title="Comentario" data-intro="Aquí puedes escribir tu comentario, pregunta o duda que tengas."></textarea>
                                        <label for="floatingTextarea" class="df_label">Comentario</label>
                                    </div>
                                    <div class="align-items-center d-none" id="loading-container">
  										<strong class="df_text">Enviando...</strong>
  										<div class="spinner-border text-info ms-auto" role="status" aria-hidden="true"></div>
									</div>
                                    <div class="row justify-content-center mb-4">
                                        <button type="sumbit" id="btn-help" class="df_btn col-11 col-sm-6 col-md-4 col-lg-2">Enviar</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
               </div>
           </div>
       </div>
    </main>
    <footer>
        
    </footer>
    <script src="js/bootstrap.bundle.min.js"></script>
</body>
</html>
