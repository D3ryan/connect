<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.Connect.models.*"%>
<%@page import="java.util.*"%>
    
<%
	
if( request.getAttribute("UserEmail") != null){
	String email = (String) request.getAttribute("UserEmail");
	pageContext.setAttribute("UserEmail", email);
}
	
if(request.getAttribute("plan") != null){
	System.out.println("Si tengo el plan");
	int plan = (int) request.getAttribute("plan");
	pageContext.setAttribute("plan", plan);
}

List<Plans_Model> PlansList = (List<Plans_Model>) request.getAttribute("PlanList");
pageContext.setAttribute("PlanList", PlansList);

%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet">
    <link href="css/general.css"        type="text/css" rel="stylesheet">
    <link href="css/navbar.css"        type="text/css" rel="stylesheet">
    <link href="css/signup.css"        type="text/css" rel="stylesheet">
    <link href="css/validation.css"        type="text/css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <script src="https://kit.fontawesome.com/ea1f7a49e6.js" crossorigin="anonymous"></script>
    <script src="js/jquery-3.6.0.min.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <script src="js/additional-methods.min.js"></script>
    <script src="js/generalValidation.js"></script>
    <title>Connect - Josue Animations</title>
</head>
<body>
    <header>
        <jsp:include page="navbar.jsp"></jsp:include>
    </header>
    <main>
        <div class="df_bgphoto min-vh-100">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="df_signup-container col-6 p-4">
                        <form action="Signup" method="post" id="signupForm" class="d-flex flex-column align-items-center">
                            <div class="image-container d-flex justify-content-center mb-3">
                                <img class="img-fluid w-50" src="img/Dark Connect logo.svg" alt="Logo Connect">
                            </div>
                            <div class="df_user-info w-100">
                                <p class="df_text text-white m-0">Crea una cuenta</p>
                                <p class="df_text text-white-50">¡Tu sitio web te espera!</p>
                                <div class="form-floating mb-3">
                                    <input type="text" name="name" class="df_input form-control" id="floatingInput" placeholder="Nombre(s)">
                                    <label for="floatingInput" class="df_label">Nombre(s)</label>
                                </div>
                                <div class="form-floating mb-3">
                                    <input type="text" name="lastname" class="df_input form-control" id="floatingInput1" placeholder="Apellido(s)">
                                    <label for="floatingInput1" class="df_label">Apellido(s)</label>
                                </div>
                                <div class="form-floating mb-3">
                                	<c:if test="${UserEmail == null}">
                                		<input type="email" name="email" class="df_input form-control" id="floatingEmail" placeholder="name@example.com">
                                    	<label for="floatingEmail" class="df_label">Correo electronico</label>
                                	</c:if>
                                    <c:if test="${UserEmail != null}">
                                		<input type="email" name="email" class="df_input form-control" id="floatingEmail" placeholder="name@example.com"  value="${ UserEmail }">
                                    	<label for="floatingEmail" class="df_label">Correo electronico</label>
                                	</c:if>
                                </div>
                                <div class="form-floating mb-3">
                                    <input type="password" name="password" class="df_input form-control" id="floatingPassword" placeholder="Password">
                                    <label for="floatingPassword" class="df_label">Contraseña</label>
                                </div>
                            </div>
                            <div class="df_billing-info w-100">
                                <p class="df_text text-white">Información de facturacion</p>
                                <p class="df_text text-white">Licencia de uso concedida a:</p>
                                <div id="usertype-container" class="row mb-3 justify-content-center">
                                    <div class="form-check form-check-inline col-4">
                                        <input class="df_radio form-check-input" name="usertype" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="Persona">
                                        <label class="form-check-label text-white" for="inlineRadio1">Persona</label>
                                    </div>
                                    <div class="form-check form-check-inline col-4">
                                        <input class="df_radio form-check-input" name="usertype" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="Compañía">
                                        <label class="form-check-label text-white" for="inlineRadio2">Compañía</label>
                                    </div>
                                </div>
                                <div class="select-container">
                                    <select class="df_text df_input df_select form-select" name="plantype" aria-label="select">
                                    	<c:if test="${plan == null}">
                                 			<option value="" selected>Tipo de plan</option>
                                 			<c:forEach var="iPlan" items="${PlanList}">
	                                     		<option value="${ iPlan.getId_Plan() }">${ iPlan.getNamePlan() } ${ iPlan.getTypePlan() } $${ iPlan.getPricePlan() } USD/m</option>
	                                     	</c:forEach>
                                 		</c:if>
                                 		
                                 		<c:if test="${plan != null}">
                                 			<option value="">Tipo de plan</option>
	                                     	<c:forEach var="iPlan" items="${PlanList}">
	                                     		<c:if test="${ plan == iPlan.getId_Plan() }">
	                                     			<option selected value="${ iPlan.getId_Plan() }">${ iPlan.getNamePlan() } ${ iPlan.getTypePlan() } $${ iPlan.getPricePlan() } USD/m</option>
	                                     		</c:if>
	                                     			<option value="${ iPlan.getId_Plan() }">${ iPlan.getNamePlan() } ${ iPlan.getTypePlan() } $${ iPlan.getPricePlan() } USD/m</option>
	                                     	</c:forEach>
                                     	</c:if>
                                    </select>
                                </div>
                                <div class="select-container">
                                    <select class="df_text df_input df_select form-select mt-3" name="country" aria-label="select">
                                        <option value="" selected>					País</option>
                                        <option value="México">						México</option>
                                        <option value="Estados Unidos">				Estados Unidos</option>
                                        <option value="Canadá">						Canadá</option>
                                        <option value="Honduras">					Honduras</option>
                                        <option value="Panamá">						Panamá</option>
                                        <option value="Colombia">					Colombia</option>
                                        <option value="Argentina">					Argentina</option>
                                        <option value="Brasil">						Brasil</option> 
                                        <option value="Ecuador">					Ecuador</option> 
                                        <option value="Chile">						Chile</option>
                                        <option value="El Salvador">				El Salvador</option> 
                                        <option value="Belice">						Belice</option>
                                    </select>
                                </div>
                                <div class="form-floating my-3">
                                    <input type="number" name="cardnumber" class="df_input df_input-number form-control" id="floatingCard" placeholder="Number">
                                    <label for="floatingCard" class="df_label">Número de tarjeta</label>
                                </div>
                                <p class="df_text text-white">Fecha de vencimiento:</p>
                                <div class="row mb-3">
                                    <div class="col-6">
                                        <select class="df_input df_select form-select col-6" name="month" aria-label="Default select example">
                                            <option value="" selected>Mes</option>
                                            <option value="1">01</option>
                                            <option value="2">02</option>
                                            <option value="3">03</option>
                                            <option value="4">04</option>
                                            <option value="5">05</option>
                                            <option value="6">06</option>
                                            <option value="7">07</option>
                                            <option value="8">08</option>
                                            <option value="9">09</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                        </select>
                                    </div>
                                    <div class="col-6">
                                        <select class="df_input df_select form-select col-6" name="year" aria-label="Default select example">
                                            <option value="" selected>Año</option>
                                            <option value="2021">2021</option>
                                            <option value="2022">2022</option>
                                            <option value="2023">2023</option>
                                            <option value="2024">2024</option>
                                            <option value="2025">2025</option>
                                            <option value="2026">2026</option>
                                            <option value="2027">2027</option>
                                            <option value="2028">2028</option>
                                            <option value="2029">2029</option>
                                            <option value="2030">2030</option>
                                            <option value="2031">2031</option>
                                            <option value="2032">2032</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-floating mb-3">
                                    <input type="number" name="securitycode" class="df_input df_input-number form-control" id="floatingCode" placeholder="Code">
                                    <label for="floatingCode" class="df_label">Código de seguridad</label>
                                </div>
                                <div class="form-floating mb-3">
                                    <input type="text" name="cardholder" class="df_input form-control" id="floatingOwner" placeholder="Owner">
                                    <label for="floatingOwner" class="df_label">Titular</label>
                                </div>
                                <div class="d-grid col-6 mb-4 mx-auto">
                                    <button type="submit" id="btn_signup" class="df_btn me-2" form="signupForm">Registrarse</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            
        </div>
    </main>
    <footer>
        
    </footer>
    <script src="js/bootstrap.bundle.min.js"></script>
</body>
</html>