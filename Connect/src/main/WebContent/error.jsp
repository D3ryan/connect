<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
	<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet">
    <link href="css/respwidth.css"        type="text/css" rel="stylesheet">
    <link href="css/general.css"        type="text/css" rel="stylesheet">
    <link href="css/navbar.css"        type="text/css" rel="stylesheet">
    <link href="css/error.css"        type="text/css" rel="stylesheet">
    <link href="css/footer.css"        type="text/css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <script src="https://kit.fontawesome.com/ea1f7a49e6.js" crossorigin="anonymous"></script>
    <title>Connect - Josue Animations</title>
<body>
<body>
    <header>
     	<jsp:include page="navbar.jsp"></jsp:include>
    </header>
    <main>
      	<div class="container-fluid min-vh-100">
            <div class="row  min-vh-100 justify-content-center align-items-center">
                <div class="col-6 df_text">
                    <span class="bug-icon text-center d-block"><i class="fas fa-bug"></i></span>
                    <h1 class="errorHeader text-center">¡Parece que hubo un error!</h1>
                    <p  class="errorSugParagraph text-center">Será mejor regresar...</p>
                    <div class="text-center">
                    	<a class="df_link return" href="index.jsp">¡Da click aquí!</a>
                    </div>
                    
                </div>
            </div>
		</div>
    </main>
    <footer class="bg-dark text-white">
          <jsp:include page="footer.jsp"></jsp:include>
    </footer>
    <script src="js/bootstrap.bundle.min.js"></script>
</body>
</html>