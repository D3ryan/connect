<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.Connect.models.*"%>
<%@page import="java.util.*"%>


<%

List<Plans_Model> PlansList = (List<Plans_Model>) request.getAttribute("PlanList");
pageContext.setAttribute("PlanList", PlansList);

%>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet">
    <link href="css/respwidth.css"        type="text/css" rel="stylesheet">
    <link href="css/general.css"        type="text/css" rel="stylesheet">
    <link href="css/navbar.css"        type="text/css" rel="stylesheet">
    <link href="css/plans.css"        type="text/css" rel="stylesheet">
    <link href="css/footer.css"        type="text/css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <script src="https://kit.fontawesome.com/ea1f7a49e6.js" crossorigin="anonymous"></script>
    <title>Connect - Josue Animations</title>
</head>
<body>
    <header>
        <jsp:include page="navbar.jsp"></jsp:include>
    </header>
    <main>
        <div class="container-fluid min-vh-100">
            <div class="row">
                <div class="col">

                    <div class="row justify-content-center mt-5">
                        <div class="col-6 d-flex justify-content-center">
                            <a class="df_link" href="#">
                               <img class="df_logo-plans d-block mx-auto d-sm-inline-block align-bottom" src="img/Iso Connect.svg" alt="Logo Connect">
                               <div class="d-sm-inline-block d-block">
                                   <span class="df_brand-plan d-block">CONNECT</span>
                                   <span class="df_subbrand d-sm-none d-block text-center">By Josue Animations</span>
                               </div>
                            </a>
                        </div>
                    </div>

                    <div class="row justify-content-center">
                        <div class="col-11">
                            <p class="df_text df-text-plan-h2 text-center my-5">Encuentra el plan adecuado para ti, manejamos varieded de opciones para las necesidades de específicas para cada usuario</p>
                        </div>
                    </div>
                    
                    <div class="row justify-content-center">
                        <div class="col-11">
                            <p class="df_plan-title text-center">Elige tu Plan</p>
                        </div>
                    </div>
                    
					<div class="row justify-content-center d-flex d-lg-none">
                        <div class="col-11">
                            <p class="df_plan-month text-center">Mensuales</p>
                        </div>
                    </div>
                    
                    <div class="row d-lg-none justify-content-center row-cols-1 row-cols-lg-3 g-4 mt-lg-1 mb-3">
                    
	                    <c:forEach var="iPlan" items="${PlanList}">
	                    
		                    <c:choose>
		                 	
		                    	<c:when test="${ iPlan.getNamePlan() == 'Básico' && iPlan.getTypePlan() == 'Mensual' }">
		                    	
			                    	<div class="col-9 col-lg-3">
			                            <div class="df_card-basic card">
			                                <div class="df_card-header df_card-header-basic card-header">
			                                  ${ iPlan.getNamePlan() }
			                                </div>
			                                <div class="card-body">
			                                    <i class="fas fa-home d-inline-block df_icon-plan me-3"></i>
			                                    <h5 class="df_card-title card-title d-inline-block">$${ iPlan.getPricePlan() } USD</h5>
			                                    <p class="card-text">por mes</p>
			                                    <div class="button-container row row-cols-1 row-cols-sm-2 justify-content-between">
			                                        <div class="col d-sm-flex justify-content-start mb-3 m-sm-0">
			                                            <a href="Signup?plan=${ iPlan.getId_Plan() }" class="df_btn df_link-btn df_btn-plan-basic">¡Contratar YA!</a>
			                                        </div>
			                                        <div class="col d-sm-flex justify-content-end">
			                                            <button class="df_btn df_btn-plan-basic" type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Mas Informacion +</button>
			                                        </div>
			                                    </div>
			                                </div>
			                            </div>
		                        	</div>
		                        	
		                        	<div class="col-9 collapse" id="collapseExample">
			                            <div class="df_info-collapsed-basic p-3">
			                                <p class="df_text df_collapsed-content">Contenido</p>
			                                <p class="df_text">- Creación de paginas ilimitadas</p>
			                                <p class="df_text">- Certificación de seguridad SSL</p> 
			                                <p class="df_text">- 3 dominios para usar</p> 
			                                <p class="df_text">- Almacenamiento de 800Gb en el host</p> 
			                                <p class="df_text">- Configuración sencilla del SEO</p> 
			                                <p class="df_text">- Informes de actividad de la página</p> 
			                                <p class="df_text">- Base de datos incluida</p> 
			                                <p class="df_text">- Conversión de las páginas a HTML</p> 
			                                <p class="df_text">- Creación de paginas ilimitadas</p>  
			                            </div>
			                        </div>
		                        	
		                    	</c:when>
		                    	
		                    	<c:when test="${ iPlan.getNamePlan() == 'Profesional' && iPlan.getTypePlan() == 'Mensual' }">
		                    		<div class="col-9 col-lg-3 mb-3">
			                        	<div class="df_card-pro card">
			                                <div class="df_card-header df_card-header-pro card-header">
			                                   ${ iPlan.getNamePlan() }
			                                </div>
			                                <div class="card-body">
			                                    <i class="fas fa-user-tie df_icon-plan me-3"></i>
				                                  <h5 class="df_card-title card-title d-inline-block">$${ iPlan.getPricePlan() } USD</h5>
				                                  <p class="card-text">por mes</p>
				                                  <div class="button-container row row-cols-1 row-cols-sm-2 justify-content-between">
			                                        <div class="col d-sm-flex justify-content-start mb-3 m-sm-0">
				                                  		<a href="Signup?plan=${ iPlan.getId_Plan() }" class="df_btn df_link-btn df_btn-plan-pro">¡Contratar YA!</a>
			                                        </div>
			                                        <div class="col d-sm-flex justify-content-end">
				                                  		<button class="df_btn df_btn-plan-pro" type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample1" aria-expanded="false" aria-controls="collapseExample">Mas Informacion +</button>
			                                        </div>
			                                  	  </div>
			                                </div>
			                            </div>
			                        </div>
			                        
			                        <div class="col-9 collapse" id="collapseExample1">
			                            <div class="df_info-collapsed-pro p-3">
			                                <p class="df_text df_collapsed-content">Contenido</p>
			                                <p class="df_text">- Creación de paginas ilimitadas</p>
			                                <p class="df_text">- Certificación de seguridad SSL</p> 
			                                <p class="df_text">- 3 dominios para usar</p> 
			                                <p class="df_text">- Almacenamiento de 800Gb en el host</p> 
			                                <p class="df_text">- Configuración sencilla del SEO</p> 
			                                <p class="df_text">- Informes de actividad de la página</p> 
			                                <p class="df_text">- Base de datos incluida</p> 
			                                <p class="df_text">- Conversión de las páginas a HTML</p> 
			                                <p class="df_text">- Creación de paginas ilimitadas</p> 
			                                <p class="df_text">- Envío de emails ilimitados</p> 
			                                <p class="df_text">- Sin marca de agua en la página</p> 
			                            </div>
			                        </div>
                        		
		                    	</c:when>
		                    	
		                    	<c:when test="${ iPlan.getNamePlan() == 'Empresarial' && iPlan.getTypePlan() == 'Mensual' }">
	                    			
	                    			<div class="col-9 col-lg-3 mb-3">
			                            <div class="df_card-ent card">
			                                <div class="df_card-header df_card-header-ent card-header">
			                                    ${ iPlan.getNamePlan() } <span class="badge bg-primary">Recomendado</span>
			                                </div>
			                                <div class="card-body">
			                                    <i class="fas fa-building df_icon-plan me-3"></i>
			                                    <h5 class="df_card-title card-title d-inline-block">$${ iPlan.getPricePlan() } USD</h5>
			                                    <p class="card-text">por mes</p>
			                                    <div class="button-container row row-cols-1 row-cols-sm-2 justify-content-between">
			                                        <div class="col d-sm-flex justify-content-start mb-3 m-sm-0">
			                                            <a href="Signup?plan=${ iPlan.getId_Plan() }" class="df_btn df_link-btn">¡Contratar YA!</a>
			                                        </div>
			                                        <div class="col d-sm-flex justify-content-end">
				                                        <button class="df_btn" type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample2" aria-expanded="false" aria-controls="collapseExample">Mas Informacion +</button>
			                                        </div>
				                               	</div>
			                                </div>
			                            </div>
			                        </div>

			                        <div class="col-9 collapse" id="collapseExample2">
			                            <div class="df_info-collapsed p-3">
			                                <p class="df_text df_collapsed-content">Contenido</p>
			                                <p class="df_text">- Creación de paginas ilimitadas</p>
			                                <p class="df_text">- Certificación de seguridad SSL</p> 
			                                <p class="df_text">- 3 dominios para usar</p> 
			                                <p class="df_text">- Almacenamiento de 800Gb en el host</p> 
			                                <p class="df_text">- Configuración sencilla del SEO</p> 
			                                <p class="df_text">- Informes de actividad de la página</p> 
			                                <p class="df_text">- Base de datos incluida</p> 
			                                <p class="df_text">- Conversión de las páginas a HTML</p> 
			                                <p class="df_text">- Creación de paginas ilimitadas</p> 
			                                <p class="df_text">- Envío de emails ilimitados</p> 
			                                <p class="df_text">- Sin marca de agua en la página</p> 
			                                <p class="df_text">- Creación de videos ilimitados</p> 
			                                <p class="df_text">- Creación de popups y barras ilimitadas</p> 
			                            </div>
			                        </div>
	                       
		                    	</c:when>

		                    </c:choose>
		                    
	                    </c:forEach>

                    </div>

                    <div class="row justify-content-center d-flex d-lg-none">
                        <div class="col-11">
                            <p class="df_plan-month text-center">Anuales</p>
                        </div>
                    </div>
                    
                    <div class="row d-lg-none justify-content-center row-cols-1 row-cols-lg-3 g-4 mt-lg-1 mb-3">
                    
	                    <c:forEach var="iPlan" items="${PlanList}">
	                    
		                    <c:choose>
		                 	
		                    	<c:when test="${ iPlan.getNamePlan() == 'Básico' && iPlan.getTypePlan() == 'Anual' }">
		                    	
			                    	<div class="col-9 col-lg-3">
			                            <div class="df_card-basic card">
			                                <div class="df_card-header df_card-header-basic card-header">
			                                  ${ iPlan.getNamePlan() }
			                                </div>
			                                <div class="card-body">
			                                    <i class="fas fa-home d-inline-block df_icon-plan me-3"></i>
			                                    <h5 class="df_card-title card-title d-inline-block">$${ iPlan.getPricePlan() } USD</h5>
			                                    <p class="card-text">por mes</p>
			                                    <div class="button-container row row-cols-1 row-cols-sm-2 justify-content-between">
			                                        <div class="col d-sm-flex justify-content-start mb-3 m-sm-0">
			                                            <a href="Signup?plan=${ iPlan.getId_Plan() }" class="df_btn df_link-btn df_btn-plan-basic">¡Contratar YA!</a>
			                                        </div>
			                                        <div class="col d-sm-flex justify-content-end">
			                                            <button class="df_btn df_btn-plan-basic" type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Mas Informacion +</button>
			                                        </div>
			                                    </div>
			                                </div>
			                            </div>
		                        	</div>
		                        	
		                        	<div class="col-9 collapse" id="collapseExample">
			                            <div class="df_info-collapsed-basic p-3">
			                                <p class="df_text df_collapsed-content">Contenido</p>
			                                <p class="df_text">- Creación de paginas ilimitadas</p>
			                                <p class="df_text">- Certificación de seguridad SSL</p> 
			                                <p class="df_text">- 3 dominios para usar</p> 
			                                <p class="df_text">- Almacenamiento de 800Gb en el host</p> 
			                                <p class="df_text">- Configuración sencilla del SEO</p> 
			                                <p class="df_text">- Informes de actividad de la página</p> 
			                                <p class="df_text">- Base de datos incluida</p> 
			                                <p class="df_text">- Conversión de las páginas a HTML</p> 
			                                <p class="df_text">- Creación de paginas ilimitadas</p>  
			                            </div>
			                        </div>
		                        	
		                    	</c:when>
		                    	
		                    	<c:when test="${ iPlan.getNamePlan() == 'Profesional' && iPlan.getTypePlan() == 'Anual' }">
		                    		<div class="col-9 col-lg-3 mb-3">
			                        	<div class="df_card-pro card">
			                                <div class="df_card-header df_card-header-pro card-header">
			                                   ${ iPlan.getNamePlan() }
			                                </div>
			                                <div class="card-body">
			                                    <i class="fas fa-user-tie df_icon-plan me-3"></i>
				                                  <h5 class="df_card-title card-title d-inline-block">$${ iPlan.getPricePlan() } USD</h5>
				                                  <p class="card-text">por mes</p>
				                                  <div class="button-container row row-cols-1 row-cols-sm-2 justify-content-between">
			                                        <div class="col d-sm-flex justify-content-start mb-3 m-sm-0">
				                                  		<a href="Signup?plan=${ iPlan.getId_Plan() }" class="df_btn df_link-btn df_btn-plan-pro">¡Contratar YA!</a>
			                                        </div>
			                                        <div class="col d-sm-flex justify-content-end">
				                                  		<button class="df_btn df_btn-plan-pro" type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample1" aria-expanded="false" aria-controls="collapseExample">Mas Informacion +</button>
			                                        </div>
			                                  	  </div>
			                                </div>
			                            </div>
			                        </div>
			                        
			                        <div class="col-9 collapse" id="collapseExample1">
			                            <div class="df_info-collapsed-pro p-3">
			                                <p class="df_text df_collapsed-content">Contenido</p>
			                                <p class="df_text">- Creación de paginas ilimitadas</p>
			                                <p class="df_text">- Certificación de seguridad SSL</p> 
			                                <p class="df_text">- 3 dominios para usar</p> 
			                                <p class="df_text">- Almacenamiento de 800Gb en el host</p> 
			                                <p class="df_text">- Configuración sencilla del SEO</p> 
			                                <p class="df_text">- Informes de actividad de la página</p> 
			                                <p class="df_text">- Base de datos incluida</p> 
			                                <p class="df_text">- Conversión de las páginas a HTML</p> 
			                                <p class="df_text">- Creación de paginas ilimitadas</p> 
			                                <p class="df_text">- Envío de emails ilimitados</p> 
			                                <p class="df_text">- Sin marca de agua en la página</p> 
			                            </div>
			                        </div>
                        		
		                    	</c:when>
		                    	
		                    	<c:when test="${ iPlan.getNamePlan() == 'Empresarial' && iPlan.getTypePlan() == 'Anual' }">
	                    			
	                    			<div class="col-9 col-lg-3 mb-3">
			                            <div class="df_card-ent card">
			                                <div class="df_card-header df_card-header-ent card-header">
			                                    ${ iPlan.getNamePlan() } <span class="badge bg-primary">Recomendado</span>
			                                </div>
			                                <div class="card-body">
			                                    <i class="fas fa-building df_icon-plan me-3"></i>
			                                    <h5 class="df_card-title card-title d-inline-block">$${ iPlan.getPricePlan() } USD</h5>
			                                    <p class="card-text">por mes</p>
			                                    <div class="button-container row row-cols-1 row-cols-sm-2 justify-content-between">
			                                        <div class="col d-sm-flex justify-content-start mb-3 m-sm-0">
			                                            <a href="Signup?plan=${ iPlan.getId_Plan() }" class="df_btn df_link-btn">¡Contratar YA!</a>
			                                        </div>
			                                        <div class="col d-sm-flex justify-content-end">
				                                        <button class="df_btn" type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample2" aria-expanded="false" aria-controls="collapseExample">Mas Informacion +</button>
			                                        </div>
				                               	</div>
			                                </div>
			                            </div>
			                        </div>

			                        <div class="col-9 collapse" id="collapseExample2">
			                            <div class="df_info-collapsed p-3">
			                                <p class="df_text df_collapsed-content">Contenido</p>
			                                <p class="df_text">- Creación de paginas ilimitadas</p>
			                                <p class="df_text">- Certificación de seguridad SSL</p> 
			                                <p class="df_text">- 3 dominios para usar</p> 
			                                <p class="df_text">- Almacenamiento de 800Gb en el host</p> 
			                                <p class="df_text">- Configuración sencilla del SEO</p> 
			                                <p class="df_text">- Informes de actividad de la página</p> 
			                                <p class="df_text">- Base de datos incluida</p> 
			                                <p class="df_text">- Conversión de las páginas a HTML</p> 
			                                <p class="df_text">- Creación de paginas ilimitadas</p> 
			                                <p class="df_text">- Envío de emails ilimitados</p> 
			                                <p class="df_text">- Sin marca de agua en la página</p> 
			                                <p class="df_text">- Creación de videos ilimitados</p> 
			                                <p class="df_text">- Creación de popups y barras ilimitadas</p> 
			                            </div>
			                        </div>
	                       
		                    	</c:when>

		                    </c:choose>
		                    
	                    </c:forEach>

                    </div>
                                  
                    <div class="row d-lg-flex d-none justify-content-center">
                        <div class="col col-lg-11 my-5">
                            <nav>
                                <div class="df_nav-tabs nav nav-tabs" id="nav-tab" role="tablist">
                                  <button class="df_nav-link nav-link active w-50" id="nav-home-tab" data-bs-toggle="tab" data-bs-target="#nav-home" type="button" role="tab" aria-controls="nav-home" aria-selected="true">Mensuales</button>
                                  <button class="df_nav-link nav-link w-50" id="nav-profile-tab" data-bs-toggle="tab" data-bs-target="#nav-profile" type="button" role="tab" aria-controls="nav-profile" aria-selected="false">Anuales <span class="df_badge badge bg-primary align-middle">Recomendado</span></button>
                                </div>
                            </nav>
                            <div class="tab-content" id="nav-tabContent">
                                <div class="df-tab-pane-left tab-pane fade show active p-5" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                    <div class="row justify-content-center">
                                        <div class="row justify-content-end col mb-4 p-0">
                                        
<!-- 											<div class="df_text col-3 p-0 d-flex justify-content-center align-items-end"> -->
<!--                                                	<p class="df_text df_compare-text">Comparativa</p> -->
<!--                                             </div>    -->
											<c:forEach var="iPlan" items="${PlanList}">
												<c:choose>
													<c:when test="${ iPlan.getNamePlan() == 'Básico' && iPlan.getTypePlan() == 'Mensual' }">
														<div class="col-3">                                            	
                                               	 			<div class="df_card-basic card">
			                                                    <div class="df_card-header df_card-header-basic card-header">
			                                                       ${ iPlan.getNamePlan() }
			                                                    </div>
			                                                    <div class="card-body">
			                                                        <i class="fas fa-home d-inline-block df_icon-plan me-3"></i>
			                                                        <h5 class="df_card-title card-title d-inline-block">$${ iPlan.getPricePlan() } USD</h5>
			                                                        <p class="card-text">por mes</p>
			                                                        <a href="Signup?plan=${ iPlan.getId_Plan() }" class="df_btn df_link-btn df_btn-plan-basic">¡Contratar YA!</a>
			                                                    </div>
                                                			</div>
                                            			</div>
													</c:when>
													<c:when test="${ iPlan.getNamePlan() == 'Profesional' && iPlan.getTypePlan() == 'Mensual' }">
														 <div class="col-3">
			                                                <div class="df_card-pro card">
			                                                    <div class="df_card-header df_card-header-pro card-header">
			                                                       ${ iPlan.getNamePlan() }
			                                                    </div>
			                                                    <div class="card-body">
			                                                        <i class="fas fa-user-tie df_icon-plan me-3"></i>
			                                                      <h5 class="df_card-title card-title d-inline-block">$${ iPlan.getPricePlan() } USD</h5>
			                                                      <p class="card-text">por mes</p>
			                                                      <a href="Signup?plan=${ iPlan.getId_Plan() }" class="df_btn df_link-btn df_btn-plan-pro">¡Contratar YA!</a>
			                                                    </div>
			                                                 </div>
                                            			</div>
													</c:when>													
													<c:when test="${ iPlan.getNamePlan() == 'Empresarial' && iPlan.getTypePlan() == 'Mensual' }">
														<div class="col-3">
		                                                	<div class="df_card-ent card">
			                                                    <div class="df_card-header df_card-header-ent card-header">
			                                                       ${ iPlan.getNamePlan() } <span class="badge bg-primary">Recomendado</span>
			                                                    </div>
			                                                    <div class="card-body">
			                                                        <i class="fas fa-building df_icon-plan me-3"></i>
			                                                        <h5 class="df_card-title card-title d-inline-block">$${ iPlan.getPricePlan() } USD</h5>
			                                                        <p class="card-text">por mes</p>
			                                                        <a href="Signup?plan=${ iPlan.getId_Plan() }" class="df_btn df_link-btn">¡Contratar YA!</a>
			                                                    </div>
		                                                  	</div>
		                                            	</div>
													</c:when>								
												</c:choose>
											</c:forEach>
                                        </div>
                               	</div>
                                </div>
                                <div class="df-tab-pane-right tab-pane fade p-5" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                    <div class="row justify-content-center">
                                        <div class="row justify-content-end col mb-4 p-0">
                                        	
<!--                                         	<div class="df_text col-3 p-0 d-flex justify-content-center align-items-end"> -->
<!--                                                	<p class="df_text df_compare-text">Comparativa</p> -->
<!--                                             </div> -->
									
                                            <c:forEach var="iPlan" items="${PlanList}">
												<c:choose>
													<c:when test="${ iPlan.getNamePlan() == 'Básico' && iPlan.getTypePlan() == 'Anual' }">
														<div class="col-3">                                            	
                                               	 			<div class="df_card-basic card">
			                                                    <div class="df_card-header df_card-header-basic card-header">
			                                                       ${ iPlan.getNamePlan() }
			                                                    </div>
			                                                    <div class="card-body">
			                                                        <i class="fas fa-home d-inline-block df_icon-plan me-3"></i>
			                                                        <h5 class="df_card-title card-title d-inline-block">$${ iPlan.getPricePlan() } USD</h5>
			                                                        <p class="card-text">por mes</p>
			                                                        <a href="Signup?plan=${ iPlan.getId_Plan() }" class="df_btn df_link-btn df_btn-plan-basic">¡Contratar YA!</a>
			                                                    </div>
                                                			</div>
                                            			</div>
													</c:when>
													<c:when test="${ iPlan.getNamePlan() == 'Profesional' && iPlan.getTypePlan() == 'Anual' }">
														 <div class="col-3">
			                                                <div class="df_card-pro card">
			                                                    <div class="df_card-header df_card-header-pro card-header">
			                                                       ${ iPlan.getNamePlan() }
			                                                    </div>
			                                                    <div class="card-body">
			                                                        <i class="fas fa-user-tie df_icon-plan me-3"></i>
			                                                      <h5 class="df_card-title card-title d-inline-block">$${ iPlan.getPricePlan() } USD</h5>
			                                                      <p class="card-text">por mes</p>
			                                                      <a href="Signup?plan=${ iPlan.getId_Plan() }" class="df_btn df_link-btn df_btn-plan-pro">¡Contratar YA!</a>
			                                                    </div>
			                                                 </div>
                                            			</div>
													</c:when>													
													<c:when test="${ iPlan.getNamePlan() == 'Empresarial' && iPlan.getTypePlan() == 'Anual' }">
														<div class="col-3">
		                                                	<div class="df_card-ent card">
			                                                    <div class="df_card-header df_card-header-ent card-header">
			                                                       ${ iPlan.getNamePlan() } <span class="badge bg-primary">Recomendado</span>
			                                                    </div>
			                                                    <div class="card-body">
			                                                        <i class="fas fa-building df_icon-plan me-3"></i>
			                                                        <h5 class="df_card-title card-title d-inline-block">$${ iPlan.getPricePlan() } USD</h5>
			                                                        <p class="card-text">por mes</p>
			                                                        <a href="Signup?plan=${ iPlan.getId_Plan() }" class="df_btn df_link-btn">¡Contratar YA!</a>
			                                                    </div>
		                                                  	</div>
		                                            	</div>
													</c:when>								
												</c:choose>
											</c:forEach>
											
                                        </div>                                       

                                    </div>
                                </div>

                            </div>
                       
							<div class="df_compare-container p-4">
								<div class="df_always-shown p-3">
<!--                                     <div class="row mb-4 p-3">	 -->
<!--                                     	<span class="df_text df_content p-0">Contenido</span>  -->
<!--                                     </div> -->

                                    <div class="df_compare-child row mb-4 p-3 justify-content-center align-items-center">	
                                        <div class="df_text col-3">
                                            <span>Creación de paginas ilimitadas</span> 
                                        </div>
                                        <div class="df_text col-3 d-flex justify-content-center">
                                            <i class="df_check fas fa-check"></i>
                                        </div>
                                        <div class="df_text col-3 d-flex justify-content-center">
                                            <i class="df_check fas fa-check"></i>
                                        </div>
                                        <div class="df_text col-3 d-flex justify-content-center">
                                            <i class="df_check fas fa-check"></i>
                                        </div>
                                    </div>
    
                                    <div class="df_compare-child row mb-4 p-3 justify-content-center align-items-center">
                                        <div class="df_text col-3 ">
                                            <span>Certificación de seguridad SSL</span> 
                                        </div>
                                        <div class="df_text col-3 d-flex justify-content-center">
                                            <i class="df_check fas fa-check"></i>
                                        </div>
                                        <div class="df_text col-3 d-flex justify-content-center">
                                            <i class="df_check fas fa-check"></i>
                                        </div>
                                        <div class="df_text col-3 d-flex justify-content-center">
                                            <i class="df_check fas fa-check"></i>
                                        </div>
                                    </div>
    
                                    <div class="df_compare-child row mb-4 p-3 justify-content-center align-items-center">
                                        <div class="df_text col-3 ">
                                            <span>Cantidad de dominios para usar</span> 
                                        </div>
                                        <div class="df_text h3 col-3 d-flex justify-content-center">
                                            3
                                        </div>
                                        <div class="df_text h3 col-3 d-flex justify-content-center">
                                            6
                                        </div>
                                        <div class="df_text h3 col-3 d-flex justify-content-center">
                                            15
                                        </div>
                                    </div>
    
                                    <div class="df_compare-child row mb-4 p-3 justify-content-center align-items-center">
                                        <div class="df_text col-3 ">
                                            <span>Almacenamiento del host de 800 GB</span>
                                        </div>
                                        <div class="df_text col-3 d-flex justify-content-center">
                                            <i class="df_check fas fa-check"></i>
                                        </div>
                                        <div class="df_text col-3 d-flex justify-content-center">
                                            <i class="df_check fas fa-check"></i>
                                        </div>
                                        <div class="df_text col-3 d-flex justify-content-center">
                                            <i class="df_check fas fa-check"></i>
                                        </div>
                                    </div>
    
                                    <div class="df_compare-child row mb-4 p-3 justify-content-center align-items-center">
                                        <div class="df_text col-3 ">
                                            <span>Configuración sencilla del SEO</span>
                                        </div>
                                        <div class="df_text col-3 d-flex justify-content-center">
                                            <i class="df_check fas fa-check"></i>
                                        </div>
                                        <div class="df_text col-3 d-flex justify-content-center">
                                            <i class="df_check fas fa-check"></i>
                                        </div>
                                        <div class="df_text col-3 d-flex justify-content-center">
                                            <i class="df_check fas fa-check"></i>
                                        </div>
                                    </div>
    
                                    <div class="df_compare-child row mb-4 p-3 justify-content-center align-items-center">
                                        <div class="df_text col-3 ">
                                            <span>Informes de actividad de la página</span>
                                        </div>
                                        <div class="df_text col-3 d-flex justify-content-center">
                                            <i class="df_check fas fa-check"></i>
                                        </div>
                                        <div class="df_text col-3 d-flex justify-content-center">
                                            <i class="df_check fas fa-check"></i>
                                        </div>
                                        <div class="df_text col-3 d-flex justify-content-center">
                                            <i class="df_check fas fa-check"></i>
                                        </div>
                                    </div>
    
                                    <div class="df_compare-child row mb-4 p-3 justify-content-center align-items-center">
                                        <div class="df_text col-3 ">
                                            <span>Base de datos incluido</span>
                                        </div>
                                        <div class="df_text col-3 d-flex justify-content-center">
                                            <i class="df_check fas fa-check"></i>
                                        </div>
                                        <div class="df_text col-3 d-flex justify-content-center">
                                            <i class="df_check fas fa-check"></i>
                                        </div>
                                        <div class="df_text col-3 d-flex justify-content-center">
                                            <i class="df_check fas fa-check"></i>
                                        </div>
                                    </div>
    
                                    <div class="df_compare-child row p-3 justify-content-center align-items-center">
                                        <div class="df_text col-3 ">
                                            <span>Conversión de las paginas a HTML</span>
                                        </div>
                                        <div class="df_text col-3 d-flex justify-content-center">
                                            <i class="df_check fas fa-check"></i>
                                        </div>
                                        <div class="df_text col-3 d-flex justify-content-center">
                                            <i class="df_check fas fa-check"></i>
                                        </div>
                                        <div class="df_text col-3 d-flex justify-content-center">
                                            <i class="df_check fas fa-check"></i>
                                        </div>
                                    </div>
                                    
                                    
                                </div>
                                
                                <div class="collapse p-3" id="collapseExample3">
                                    <div class="df_compare-child row mb-4 p-3 justify-content-center align-items-center">
                                        <div class="df_text col-3 ">
                                            <span>Envío de emails ilimitados</span>
                                        </div>
                                        <div class="df_text col-3 d-flex justify-content-center">
                                        </div>
                                        <div class="df_text col-3 d-flex justify-content-center">
                                            <i class="df_check fas fa-check"></i>
                                        </div>
                                        <div class="df_text col-3 d-flex justify-content-center">
                                            <i class="df_check fas fa-check"></i>
                                        </div>
                                    </div>

                                    <div class="df_compare-child row mb-4 p-3 justify-content-center align-items-center">
                                        <div class="df_text col-3 ">
                                            <span>Sin marcas de agua en la página</span>
                                        </div>
                                        <div class="df_text col-3 d-flex justify-content-center">
                                        </div>
                                        <div class="df_text col-3 d-flex justify-content-center">
                                            <i class="df_check fas fa-check"></i>
                                        </div>
                                        <div class="df_text col-3 d-flex justify-content-center">
                                            <i class="df_check fas fa-check"></i>
                                        </div>
                                    </div>

                                    <div class="df_compare-child row mb-4 p-3 justify-content-center align-items-center">
                                        <div class="df_text col-3 ">
                                            <span>Sin marcas de agua en la página</span>
                                        </div>
                                        <div class="df_text col-3 d-flex justify-content-center">
                                        </div>
                                        <div class="df_text col-3 d-flex justify-content-center">
                                            <i class="df_check fas fa-check"></i>
                                        </div>
                                        <div class="df_text col-3 d-flex justify-content-center">
                                            <i class="df_check fas fa-check"></i>
                                        </div>
                                    </div>

                                    <div class="df_compare-child row mb-4 p-3 justify-content-center align-items-center">
                                        <div class="df_text col-3">
                                            <span>Creación de videos interactivos ilimitados</span>
                                        </div>
                                        <div class="df_text col-3 d-flex justify-content-center">
                                        </div>
                                        <div class="df_text col-3 d-flex justify-content-center">
                                        </div>
                                        <div class="df_text col-3 d-flex justify-content-center">
                                            <i class="df_check fas fa-check"></i>
                                        </div>
                                    </div>              
                                    
                                    <div class="df_compare-child row mb-4 p-3 justify-content-center align-items-center">
                                        <div class="df_text col-3">
                                            <span>Creación de pop ups y barras ilimitadas</span>
                                        </div>
                                        <div class="df_text col-3 d-flex justify-content-center">
                                        </div>
                                        <div class="df_text col-3 d-flex justify-content-center">
                                        </div>
                                        <div class="df_text col-3 d-flex justify-content-center">
                                            <i class="df_check fas fa-check"></i>
                                        </div>
                                    </div>     
                                </div>

                                <div class="df_button-container d-flex justify-content-center">
                                    <button class="df_btn w-25 mb-4" type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample3" aria-expanded="false" aria-controls="collapseExample">
                                        Ver mas
                                    </button>
                                </div>

                            </div>

						</div>
                    </div>
                    
                    
                </div>
            </div>

            

        </div>
    </main>
    <footer class="bg-dark text-white">
          <jsp:include page="footer.jsp"></jsp:include>
    </footer>
    <script src="js/bootstrap.bundle.min.js"></script>
</body>
</html>
