<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.Connect.models.*"%>
<%@page import="java.util.*"%>

<%
	
Users_Model selectedUser = (Users_Model)request.getAttribute("SelectedUser");
pageContext.setAttribute("SelectedUser", selectedUser);

String promoType = (String) request.getAttribute("PromoType");

if(promoType != null){
	List<Plans_Model> planPromo = (List<Plans_Model>) request.getAttribute("PromoPlans");
	pageContext.setAttribute("PromoPlans", planPromo);
}


%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/bootstrap.min.css" 		  type="text/css" rel="stylesheet">
    <link href="css/general.css"       		  type="text/css" rel="stylesheet">
    <link href="css/navbar.css"        		  type="text/css" rel="stylesheet">
    <link href="css/dash-general.css"         type="text/css" rel="stylesheet">
    <link href="css/introjs.css"         	  type="text/css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <script src="https://kit.fontawesome.com/ea1f7a49e6.js" crossorigin="anonymous"></script>
    <script src="js/jquery-3.6.0.min.js"></script>
    <script src="js/intro.js"></script>
    <title>Connect - Josue Animations</title>
</head>
<body>
    <header>
      <jsp:include page="navbar.jsp"></jsp:include>
    </header>
    <main>
        <div class="container-fluid">
          <div class="row flex-column flex-sm-row min-vh-100">
              <div class="df_dashnav col-12 col-sm-2 p-0">
                   <jsp:include page="dash-navbar.jsp"></jsp:include>
              </div>
              <div class="col-12 col-sm-10">
                <div class="row my-5 my-sm-5">
                    <div class="col d-flex flex-column align-items-center">
                        <p class="df_text df_welcome-gral text-center text-sm-start" data-step="1" data-title="¡Bienvenido!" data-intro="Aquí estará siempre tu nombre.">Bienvenido, ¡${SelectedUser.getFirstNameUser()}!</p>
                        
                        <c:if test="${SelectedUser.isStatusUser() == true}">
	                        <p class="df_text df_subsciption text-center text-sm-start d-inline-block mt-2 mb-0 px-2 py-1" data-step="2" data-title="Tu suscripción" data-intro="Aquí se te mostrará el estado de tu suscripción.">
	                        	Suscripción: Activa
	                        </p>
	                    </c:if>
	                    <c:if test="${SelectedUser.isStatusUser() == false}">
	                    	<p class="df_text df_subsciption text-center text-sm-start d-inline-block mt-2 mb-0 px-2 py-1" data-step="2" data-title="Tu suscripción" data-intro="Aquí se te mostrará el estado de tu suscripción.">
	                        	Suscripción: Inactiva
	                        </p>
	                    </c:if>
                        <!-- <p class="d-inline-block df_text">Activa</p> -->
                    </div>
                </div>
                <div class="row row-cols-1 row-cols-sm-2 mb-5">
                    <div class="col pe-sm-5 mb-5 mb-sm-3 d-flex flex-column align-items-center">
                      <div class="df_payment-date p-3 d-flex flex-column align-items-center align-self-sm-end"  data-step="3" data-title="El día de pago" data-intro="Siempre te mantendremos actualizado de la fecha de cobro">
                        <p class="df_text df_text-gral">Siguiente corte:</p>
                        <span class="df_text df_little-text">${SelectedUser.getCutoffDateToString()}</span>
                      </div>
                    </div>
                    <div class="col ps-sm-5 d-flex flex-column align-items-center">
                      <div class="df_payment-date p-3 d-flex flex-column align-items-center align-self-sm-start" data-step="4" data-title="Tipo de plan" data-intro="Siempre mostraremos tu tipo de plan">
                        <p class="df_text df_text-gral">Tipo de plan:</p>
                        <span class="df_text df_little-text">${SelectedUser.getNamePlan()} ${SelectedUser.getTypePlan()}</span>
                      </div> 
                    </div>
                </div>
                <!-- Best Plan  -->
                
                <c:if test="${PromoType == 'BP'}">
                <div class="p-3">
					<div class="df_recomendation row justify-content-center" data-step="5" data-title="Plan mas comprado" data-intro="Aquí te presentamos nuestro plan mas comprado">
	                    <h1 class="df_text df_text-recomendation text-center">¡Nuestro plan mas comprado!</h1>
	                    <p class="df_text text-center">¡Pensamos que podrías mejorar tu plan con el mas comprado, actualiza el tuyo ahora mismo!</p>
	                    <div class="row justify-content-center g-2 mb-5">
	                        <div class="col-11 col-sm-8">
	                          <div class="df_card card text-white">
	                            <div class="card-body">
	                            
	                              <h5 class="card-title df_text">${ PromoPlans.get(0).getNamePlan() } ${ PromoPlans.get(0).getTypePlan() } </h5>
	                              <p class="card-text df_text">Te presentamos este plan para que mejores el que tienes, mucha gente con tu plan lo ha preferido, ¡no te quedes atras!</p>
	                              <p class="card-text df_text">Por solo $${ PromoPlans.get(0).getPricePlan() } USD al mes</p>
	                              <a href="DashAccount?plan=${ PromoPlans.get(0).getId_Plan() }" class="df_btn df_link-btn">Actualizar plan</a>
	                            </div>
	                          </div>
	                        </div>
	                    </div>
		            </div>  
		        </div>                       	
		        </c:if>
		        <!-- Next 3  -->
		         <c:if test="${PromoType == 'N3'}">
		         	
		         	<div class="p-3">
		         		<div class="df_recomendation row justify-content-center" data-step="5" data-title="¡Mejora tu plan!" data-intro="En esta parte te mostraremos opciones para mejorar tu plan, ¿por qué no hacerlo ahora mismo?">
		                    <h1 class="df_text df_text-recomendation text-center">¡Mejora tu plan!</h1>
		                    <p class="df_text text-center">¡Creemos que estos planes te podrian interesar!</p>
		               		<c:forEach var="iPromoPlans" items="${PromoPlans}">
			                    <div class="row justify-content-center g-2 mb-5">
			                        <div class="col-11 col-sm-8">
			                          <div class="df_card card text-white">
			                            <div class="card-body">
				                              <h5 class="card-title df_text">${ iPromoPlans.getNamePlan() } ${ iPromoPlans.getTypePlan() } </h5>
				                              <p class="card-text df_text">Mejora tu plan por solo</p>
				                              <p class="card-text df_text">$${ iPromoPlans.getPricePlan() } USD al mes</p>
				                              <a href="DashAccount?plan=${ iPromoPlans.getId_Plan() }" class="df_btn df_link-btn">Actualizar plan</a>
			                            </div>
			                          </div>
			                        </div>
			                    </div>
		                    </c:forEach>
		            	</div>
		         	</div>
		         	
		       
					       	
		        </c:if>
		        
              </div>
          </div>
        </div>
    </main>
    <footer>
        
    </footer>
    <script src="js/bootstrap.bundle.min.js"></script>
</body>
</html>