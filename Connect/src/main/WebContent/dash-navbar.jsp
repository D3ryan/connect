<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<div class="d-sm-none">
	<a class="df_link d-block m-2" data-bs-toggle="collapse" href="#collapseNavbar" role="button" aria-expanded="false" aria-controls="collapseNavbar">
    Menú
 	</a>
	<div class="collapse" id="collapseNavbar">
		<nav class="nav d-sm-none flex-sm-column">
		    <a class="nav-link df_link df_link-dash w-100" href="https://www.dotcompal.com/login" target="_blank"><i class="fas fa-globe me-2"></i>Aplicación</a>
		    <a class="nav-link df_link df_link-dash w-100" href="DashGeneral"><i class="fas fa-chalkboard-teacher me-2"></i>General</a>
		    <a class="nav-link df_link df_link-dash w-100" href="DashPayment"><i class="fas fa-dollar-sign me-2"></i>Pagos</a>
		    <a class="nav-link df_link df_link-dash w-100" href="DashAccount"><i class="fas fa-address-card me-2"></i>Cuenta y Planes</a>
		    <a class="nav-link df_link df_link-dash w-100" href="dash-help.jsp"><i class="fas fa-question-circle me-2"></i>Soporte</a>
		    <a class="nav-link df_link df_link-dash w-100" href="javascript:introJs().setOption('showProgress', true).start();"><i class="fas fa-info-circle me-2"></i>Ayuda</a>
		</nav>
	</div>
</div>

 

    
<nav class="nav d-none d-sm-flex flex-sm-column">
    <a class="nav-link df_link df_link-dash w-100" href="https://www.dotcompal.com/login" target="_blank"><i class="fas fa-globe me-2"></i>Aplicación</a>
    <a class="nav-link df_link df_link-dash w-100" href="DashGeneral"><i class="fas fa-chalkboard-teacher me-2"></i>General</a>
    <a class="nav-link df_link df_link-dash w-100" href="DashPayment"><i class="fas fa-dollar-sign me-2"></i>Pagos</a>
    <a class="nav-link df_link df_link-dash w-100" href="DashAccount"><i class="fas fa-address-card me-2"></i>Cuenta y Planes</a>
    <a class="nav-link df_link df_link-dash w-100" href="dash-help.jsp"><i class="fas fa-question-circle me-2"></i>Soporte</a>
    <a class="nav-link df_link df_link-dash w-100" href="javascript:introJs().setOption('showProgress', true).start();"><i class="fas fa-info-circle me-2"></i>Ayuda</a>
</nav>