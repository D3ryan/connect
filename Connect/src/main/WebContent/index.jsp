<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet">
    <link href="css/respwidth.css"        type="text/css" rel="stylesheet">
    <link href="css/general.css"        type="text/css" rel="stylesheet">
    <link href="css/navbar.css"        type="text/css" rel="stylesheet">
    <link href="css/index.css"        type="text/css" rel="stylesheet">
    <link href="css/footer.css"        type="text/css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <script src="https://kit.fontawesome.com/ea1f7a49e6.js" crossorigin="anonymous"></script>
    <script src="js/jquery-3.6.0.min.js"></script>
    <script src="js/jquery.scrollme.min.js"></script>
    <title>Connect - Josue Animations</title>
</head>
<body>
    <header>
     	<jsp:include page="navbar.jsp"></jsp:include>
    </header>
    <main>
        <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-indicators">
              <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
              <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
              <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
            </div>
            <div class="carousel-inner">
              <div class="carousel-item active">
                  <img src="img/slide1.jpg" class="df_image d-block w-100" alt="...">
                  <div class="carousel-caption mb-4">
                    <h5 class="df_text df_text-carrousel mb-3">¡Empieza tu sitio web ahora mismo!</h5>
                    <a href="Plans" class="df_btn df_btn-red df_link-btn">¡Contrata Ya!</a>
                  </div>
              </div>
              <div class="carousel-item">
                  <img src="img/slide2.jpg" class="d-none d-sm-block df_image df_slide-2 d-block w-100" alt="...">
                  <img src="img/index2mobile.jpg" class="d-sm-none d-block df_image df_slide-2 d-block w-100" alt="...">
                  <div class="carousel-caption mb-4">
                    <h5 class="df_text df_text-carrousel mb-3">¡Aumenta tus ventas en la web!</h5>
                    <a href="Plans" class="df_btn df_btn-red df_link-btn">¡Contrata Ya!</a>
                  </div>
              </div>
              <div class="carousel-item">
                  <img src="img/slide3.jpg" class="d-none d-sm-block df_image df_slide-3 d-block w-100" alt="...">
                  <img src="img/index3mobile.jpg" class="d-sm-none d-block df_image df_slide-2 d-block w-100" alt="...">
                  <div class="carousel-caption mb-4">
                    <h5 class="df_text df_text-carrousel mb-3">¡Crea contenido responsivo!</h5>
                    <a href="Plans" class="df_btn df_btn-red df_link-btn">¡Contrata Ya!</a>
                  </div>
              </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>
        <div class="container-fluid pb-5">

          <div class="df_promo-contenedor row align-items-center my-5">
            <div class="col">
              <div class="row">
                <div class="col-12">
                  <p class="df_promo text-center mb-4"><span class="df_big-promo">15</span> dias <span class="df_big-promo">GRATIS</span> al contratar!</p>
                </div>
              </div>
              <div class="row justify-content-sm-center">
                <div class="col-md-6">
                  <form action="Index" method="post" class="input-group my-2">
                      <input type="email" name="email" class="form-control df_input" placeholder="Ingresa tu email" aria-label="email">
                      <button class="df_btn" type="submit">Obtener</button>
                 </form>
                </div>
              </div>
            </div>
          </div>
          
          <div class="row justify-content-center">
            <div class="col-9 col-md-4 col-lg-5">
                <p class="df_plans text-center text-white">Nuestros planes</p>
            </div>
          </div>

          <div class="row justify-content-center row-cols-1 row-cols-lg-3 g-4 mt-lg-1 mb-3">
            <div class="col-9 col-lg-3">
              <div class="df_card-basic card">
                <div class="df_card-header df_card-header-basic card-header">
                  Basico
                </div>
                <div class="card-body">
                  <p class="card-text df_recommended text-center text-white-50">Recomendado para</p>
                  <h5 class="df_card-title card-title text-center mb-4">Usuarios individuales</h5>
                  <p class="card-text text-white-50 mb-4">Crea paginas de manera ilimitada con ciertas limitaciones visuales como marca de agua</p>
                  <a href="Plans" class="df_btn df_link-btn df_btn-plan-basic">¡Ver plan!</a>
              </div>
              </div>
            </div>

            <div class="col-9 col-lg-3 mb-3">
              <div class="card df_card-pro">
                <div class="df_card-header df_card-header-pro card-header">
                  Profesional
                </div>
                <div class="card-body">
                  <p class="card-text df_recommended text-center text-white-50">Recomendado para</p>
                  <h5 class="df_card-title card-title text-center mb-4">Equipos pequeños</h5>
                  <p class="card-text text-white-50 mb-4">Crea paginas de manera ilimitada con funciones profesionales como analitícas del sitio</p>
                  <a href="Plans" class="df_btn df_link-btn df_btn-plan-pro">¡Ver plan!</a>
              </div>
              </div>
            </div>

            <div class="col-9 col-lg-3 mb-3">
              <div class="df_card-ent card">
                <div class="df_card-header df_card-header-ent card-header">
                    Empresarial
                </div>
                <div class="card-body">
                  <p class="card-text df_recommended text-center text-white-50">Recomendado para</p>
                  <h5 class="df_card-title card-title text-center mb-4">Equipos grandes</h5>
                  <p class="card-text text-white-50 mb-4">Crea paginas de manera ilimitada con funciones profesionales y protocolos de seguridad</p>
                    <a href="Plans" class="df_btn df_link-btn">¡Ver plan!</a>
                </div>
              </div>
            </div>

          </div> 

          <div class="row justify-content-center mb-2">
            <div class="col-9 col-md-4 col-lg-5">
                <p class="df_benefits-title text-center text-white">Beneficios al suscribirse</p>
            </div>
          </div>

          <div class="row row-cols-1 row-cols-md-12 justify-content-center mb-3">
            <div class="col-12 col-lg-9 col-xl-8 row justify-content-center ">
              <div class="col-12 col-md-6 mb-3 scrollme">
                <div class="df_img-container scrollme">
                  <img class="img-fluid df_img animateme" data-when="enter" data-from="1.0" data-to="0.0" data-opacity="0"  data-translatey="150" src="img/index1.jpg" alt="index1">
                </div>
              </div>
              <div class="col-12 col-md-6 d-flex justify-content-center align-items-center scrollme">
                <p class="df_text df_benefits text-center animateme" data-when="enter" data-from="1.0" data-to="0.0" data-opacity="0"  data-translatey="-150">Optimiza tu desarrollo en equipo</p>
              </div>
            </div>
          </div>

          <div class="row row-cols-1 row-cols-md-12 justify-content-center mb-3">
            <div class="col-12 col-lg-9 col-xl-8 row justify-content-center">
              <div class="col-12 col-md-6 d-none d-sm-flex justify-content-center align-items-center scrollme">
                <p class="df_text df_benefits text-center animateme" data-when="enter" data-from="1.0" data-to="0.0" data-opacity="0"  data-translatey="150">Olvídate de la codificación</p>
              </div>
              <div class="col-12 col-md-6">
                <div class="df_img-container scrollme">
                  <img class="img-fluid df_img animateme" data-when="enter" data-from="1.0" data-to="0.0" data-opacity="0"  data-translatey="-100" src="img/index2.jpg" alt="index2">
                </div>
              </div>

              <div class="col-12 mt-3 d-sm-none d-flex  justify-content-center align-items-center scrollme">
                <p class="df_text df_benefits text-center animateme" data-when="enter" data-from="1.0" data-to="0.0" data-opacity="0"  data-translatey="150">Olvídate de la codificación</p>
              </div>
            </div>
          </div>

          <div class="row row-cols-1 row-cols-md-12 justify-content-center">
            <div class="col-12 col-lg-9 col-xl-8 row justify-content-center">
              <div class="col-12 col-md-6">
                <div class="df_img-container scrollme">
                  <img class="img-fluid df_img animateme" src="img/index3.jpg" alt="index3" data-when="enter" data-from="1.0" data-to="0.0" data-opacity="0"  data-translatey="150">
                </div>
              </div>
              <div class="col-12 col-md-6 mt-3 d-flex justify-content-center align-items-center scrollme">
                <p class="df_text df_benefits text-center animateme" data-when="enter" data-from="1.0" data-to="0.0" data-opacity="0"  data-translatey="-150">Mejora tu imagen ante los clientes</p>
              </div>
            </div>
          </div>
        </div>       
            
    </main>
    <footer class="bg-dark text-white">
          <jsp:include page="footer.jsp"></jsp:include>
    </footer>
    <script src="js/bootstrap.bundle.min.js"></script>
</body>
</html>