<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.util.*"%>    

<%

String userFound = (String) request.getAttribute("userFound");
pageContext.setAttribute("userFound", userFound);

%>
    
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet">
    <link href="css/respwidth.css"        type="text/css" rel="stylesheet">
    <link href="css/general.css"        type="text/css" rel="stylesheet">
    <link href="css/navbar.css"        type="text/css" rel="stylesheet">
    <link href="css/login.css"        type="text/css" rel="stylesheet">
    <link href="css/validation.css"        type="text/css" rel="stylesheet">
    <link href="css/footer.css"        type="text/css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <script src="https://kit.fontawesome.com/ea1f7a49e6.js" crossorigin="anonymous"></script>
    <script src="js/jquery-3.6.0.min.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <script src="js/additional-methods.min.js"></script>
    <script src="js/generalValidation.js"></script>
    <title>Connect - Josue Animations</title>
</head>
<body>
    <header>
    	<jsp:include page="navbar.jsp"></jsp:include>
    </header>
    <main>
        <div class="min-vh-100 container-fluid pt-4 ps-md-0 pt-md-0">
           <div class="min-vh-100 row">
               <div class="col-md-6 d-none d-md-block">
                    <img src="img/loginImg2.jpg" alt="" class=" min-vh-100 img-fluid">
               </div>
               <div class="col-md-6 d-md-flex justify-content-md-center align-items-md-center">
                    <form action="Login" id="LoginForm" method="post" class="h-100 w-md-75 w-lg-75 w-xl-75 d-flex flex-column justify-content-md-center align-items-md-center">
                        <div class="mb-3 mt-md-auto mb-md-auto mt-md-4 d-flex flex-column align-items-center">
                            <a class="df_link df_brand-login smx-auto" href="index.jsp"><img class="df_logo-login d-inline-block align-top" src="img/Iso Connect.svg" alt="Logo Connect">CONNECT</a>
                            <p class="df_text df_text-welcome text-center">¡Bienvenido de nuevo!</p>
                        </div>
                        <div class="mb-3 w-md-100 w-lg-100 w-xl-100">
	                        <label for="exampleInputEmail1" class="df_label form-label">Correo electronico</label>
	                        <input type="email" name="email" class="df_input form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="ejemplo@dominio.com">
                        </div>
                        <div class="mb-3 w-md-100 w-lg-100 w-xl-100">
	                        <label for="exampleInputPassword1" class="df_label form-label">Contraseña</label>
	                        <input type="password" name="password" class="df_input form-control" id="exampleInputPassword1" placeholder="Ingresa tu contraseña">
	                        <c:if test="${userFound != null}">
		         				<label id="exampleInputPassword1-error" class="error" for="exampleInputPassword1">El correo electrónico y su contraseña no coinciden con alguna cuenta</label>
		         			</c:if>
                        </div>
                        <div class="mb-3 w-md-100 w-lg-100 w-xl-100">
                            <a class="df_link df_link-forgotten">¿Olvidaste tu contraseña?</a>
                        </div>
                        <div class="mb-3 w-md-100 w-lg-100 w-xl-100 row justify-content-center">
                            <div class="col-12 col-sm-6">
                                <button type="submit" form="LoginForm" id="btn_login" class="df_btn w-100">Ingresar</button>
                            </div>
                        </div>
                        <div class="my-auto mb-xxl-5 w-md-100 w-lg-100 w-xl-100">
                            <p class="df_text df_vertical-align-sub text-center">¿Aún no tienes una cuenta? <a href="Signup" class="df_link df_link-register d-inline-block">¡Registrate YA!</a></p>
                        </div>
                    </form>
               </div>
           </div>
        </div>
    </main>
    <footer class="bg-dark text-white">
          <jsp:include page="footer.jsp"></jsp:include>
    </footer>
    <script src="js/bootstrap.bundle.min.js"></script>
</body>
</html>
