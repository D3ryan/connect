<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.Connect.models.*"%>
<%@page import="java.util.*"%>
    
    
<%

Users_Model selectedUser = (Users_Model)request.getAttribute("SelectedUser");
pageContext.setAttribute("SelectedUser", selectedUser);


List<Payments_Model> payments = (List<Payments_Model>) request.getAttribute("PaymentsUser");

pageContext.setAttribute("PaymentsUser", payments);

%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/bootstrap.min.css" 	   type="text/css" rel="stylesheet">
    <link href="css/respwidth.css"         type="text/css" rel="stylesheet">
    <link href="css/general.css"           type="text/css" rel="stylesheet">
    <link href="css/navbar.css"            type="text/css" rel="stylesheet">
    <link href="css/dash-payment.css"      type="text/css" rel="stylesheet">
    <link href="css/introjs.css"         	  type="text/css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <script src="https://kit.fontawesome.com/ea1f7a49e6.js" crossorigin="anonymous"></script>
    <script src="js/jquery-3.6.0.min.js"></script>
    <script src="js/intro.js"></script>
    <script src="js/payments.js"></script>
    <title>Connect - Josue Animations</title>
</head>
<body>
    <header>
        <jsp:include page="navbar.jsp"></jsp:include>
    </header>
    <main>
       <div class="container-fluid">
           <div class="row flex-column flex-sm-row min-vh-100">
               <div class="df_dashnav col-12 col-sm-2 p-0">
                  	<jsp:include page="dash-navbar.jsp"></jsp:include>
               </div>
               <div class="col-12 col-sm-10">
                   <div class="row justify-content-center">
                       <div class="col-11">
                            <div class="row">
                                <div class="col">
                                    <p class="df_text df_text-payment">Pagos</p>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <form method="post" action="DashPayment" id="filter-form"  class="df_filter-controls row row-cols-1 flex-sm-column justify-content-center p-0">
                                    <div class="col p-0 col-sm-4 col-lg-2 mb-3">
                                        <button class="df_btn w-100" type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample"
                                        data-step="1" data-title="Filtrar" data-intro="Da clic aquí para filtrar tus pagos por año y mes.">
                                        Filtrar
                                        </button>
                                    </div>
                                    <div class="col row p-0 justify-content-center align-self-sm-center collapse" id="collapseExample">                                        
                                        <div class="col-sm-6 p-0 pe-sm-2">
                                            <select class="df_text df_input df_select form-select mb-3" id="yearSelect" aria-label="select" name="year">
                                                <option value="0" selected>Año</option>
                                                <option value="2020">2020</option>
                                                <option value="2021">2021</option>
                                                <option value="2022">2022</option>
                                                <option value="2023">2023</option>
                                                <option value="2024">2024</option>
                                                <option value="2025">2025</option>
                                                <option value="2026">2026</option>
                                                <option value="2027">2027</option>
                                                <option value="2028">2028</option>
                                                <option value="2029">2029</option>
                                                <option value="2030">2030</option>
                                                <option value="2031">2031</option>
                                                <option value="2032">2032</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-6 p-0 ps-sm-2">
                                            <select class="df_text df_input df_select form-select mb-3" id="monthSelect" aria-label="select" name="month">
                                                <option value="0" selected>Mes</option>
                                                <option value="1">Enero</option>
                                                <option value="2">Febrero</option>
                                                <option value="3">Marzo</option>
                                                <option value="4">Abril</option>
                                                <option value="5">Mayo</option>
                                                <option value="6">Junio</option>
                                                <option value="7">Julio</option>
                                                <option value="8">Agosto</option>
                                                <option value="9">Septiembre</option>
                                                <option value="10">Octubre</option>
                                                <option value="11">Noviembre</option>
                                                <option value="12">Diciembre</option>
                                            </select>
                                        </div>
                                    </div>
                                
                                </form>
                            </div>                       
                            <div class="row justify-content-center">
                                <div class="df_date-payment col p-2 mb-3"  data-step="2" data-title="Lista de pagos" data-intro="Aquí te mostraremos todos los movimientos a tu tarjeta">
                        			<c:forEach var="iPayment" items="${PaymentsUser}">
                                		<p class="df_payment df_text">
                                		${iPayment.getBeginDatePayment()} a ${iPayment.getCutoffDatePayment()} 
                                		Plan ${iPayment.getNamePlan()} ${iPayment.getTypePlan()} 
                                		$ ${iPayment.getAmountPayment()} USD
                                		<c:if test="${ iPayment.getStatusPayment() == false}"> Sin cobrar </c:if>
                                		</p>	
                                	</c:forEach>
                                </div>
                            </div>
                        </div>
                   </div>
                    
               </div>
           </div>
       </div>
    </main>
    <footer>
        
    </footer>
    <script src="js/bootstrap.bundle.min.js"></script>
</body>
</html>
