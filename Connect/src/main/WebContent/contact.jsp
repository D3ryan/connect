<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet">
    <link href="css/respwidth.css"        type="text/css" rel="stylesheet">
    <link href="css/general.css"        type="text/css" rel="stylesheet">
    <link href="css/navbar.css"        type="text/css" rel="stylesheet">
    <link href="css/contact.css"        type="text/css" rel="stylesheet">
    <link href="css/validation.css"        type="text/css" rel="stylesheet">
    <link href="css/jquery.sweet-modal.min.css" type="text/css" rel="stylesheet" >
    <link href="css/footer.css"        type="text/css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <script src="https://kit.fontawesome.com/ea1f7a49e6.js" crossorigin="anonymous"></script>
    <script src="js/jquery-3.6.0.min.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <script src="js/additional-methods.min.js"></script>
    <script src="js/jquery.sweet-modal.min.js"></script>
    <script src="js/generalValidation.js"></script>
    <title>Connect - Josue Animations</title>
</head>
<body>
    <header>
    	<jsp:include page="navbar.jsp"></jsp:include>
    </header>
    <main>
        <div class="df_img-background container-fluid min-vh-100">
            <div class="row justify-content-center align-items-center">
                <div class="df_container col-11 col-lg-10 col-xl-8 row my-5 p-0">
                    <div class="col col-md-5 col-lg-4 d-none d-sm-block p-0">
                        <div class="df_img-container h-100 d-flex">
                            <img class="df_img img-fluid" src="img/contact.jpg" alt="Mujer Contacto">
                        </div>
                    </div>
                    <div class="col col-md-7 col-lg-8 p-sm-5">
                        <form class="d-flex flex-column h-100" id="contactForm" action="DashContact" method="post">
                            <div class="df_text-container mb-3">
                                <a class="df_link d-flex flex-column flex-sm-row justify-content-center align-items-center" href="#">
                                    <img class="df_logo-contact align-bottom me-sm-2" src="img/Iso Connect.svg" alt="Logo Connect">
                                    <div class="d-sm-inline-block d-block">
                                        <span class="df_brand-contact d-block">CONNECT</span>
                                        <span class="df_subbrand d-sm-none d-block text-center">By Josue Animations</span>
                                    </div>
                                </a>
                            </div>
                            <p class="df_text df_text-contact-p1 text-center">¿Aún tienes dudas de nuestro servicio?</p>
                            <p class="df_text df_text-contact-p2 text-white-50 text-center">Síguenos en nuestras redes sociales para mas información</p>
                            <div class="df_social d-flex justify-content-evenly mb-3">
                                <a href="https://www.instagram.com/josueanimations/" target="_blank" rel="noopener noreferrer"><i class="df_instagram fab fa-instagram"></i></a> 
                                <a href="https://www.facebook.com/josueanimationsoficial" target="_blank" rel="noopener noreferrer"><i class="df_facebook fab fa-facebook-square"></i></a>
                                <a href="https://m.me/josueanimationsoficial" target="_blank" rel="noopener noreferrer"><i class="df_messenger fab fa-facebook-messenger"></i></a>
                            </div>
                            <p class="df_text df_text-lines text-center d-flex">o</p>
                            <p class="df_text df_text-contact-p2 text-center text-white-50">¡Mandanos tus preguntas!</p>
                            <div class="form-floating mb-3">
                                <input type="email" class="df_input form-control" id="floatingEmail" placeholder="name@example.com" name="email">
                                <label for="floatingEmail" class="df_label">Correo electrónico</label>
                            </div>
                            <div class="form-floating mb-3">
                                <input type="text" class="df_input form-control" id="floatingSubject" placeholder="Subject" name="subject">
                                <label for="floatingSubject" class="df_label">Asunto</label>
                            </div>
                            <div class="form-floating flex-grow-1 mb-3 df_textarea-contact">
                                <textarea class="df_input form-control h-100" placeholder="Comentario" id="floatingTextarea" name="comment"></textarea>
                                <label for="floatingTextarea" class="df_label">Comentario</label>
                            </div>
							<div class="align-items-center d-none" id="loading-container">
  								<strong class="df_text">Enviando...</strong>
  								<div class="spinner-border text-info ms-auto" role="status" aria-hidden="true"></div>
							</div>
                            <div class="row justify-content-center mt-4">
                                <button type="submit" id="btn-contact" class="df_btn col-11 col-sm-6 col-lg-3">Enviar</button>
                                </div>
                        </form>
                    </div>
                </div>  
            </div>
        </div>
    </main>
   	<footer class="bg-dark text-white">
          <jsp:include page="footer.jsp"></jsp:include>
    </footer>
    <script src="js/bootstrap.bundle.min.js"></script>
</body>
</html>