$(document).ready(function(){

    $("#yearSelect").change(function(e) { 
        e.preventDefault();
        var yearSelect = $("#yearSelect").val();
        var monthSelect = $("#monthSelect").val();

        $.ajax({
            url: "DashPayment",
            type: "POST",
            data:  {
                year: yearSelect,
                month: monthSelect
            },
            success: function (response) {
                $(".df_date-payment").hide().html(response).fadeIn("slow");
            },
            error: function (response) {  
                alert("Hubo un error");
            },
            failure: function (response){
                alert("Hubo un error");
            }
        });
    });

    $("#monthSelect").change(function(e) { 
        e.preventDefault();
        var yearSelect = $("#yearSelect").val();
        var monthSelect = $("#monthSelect").val();

        $.ajax({
            url: "DashPayment",
            type: "POST",
            data:  {
                year: yearSelect,
                month: monthSelect
            },
            success: function (response) {
                $(".df_date-payment").hide().html(response).fadeIn("slow");
            },
            error: function (response) {  
                alert("Hubo un error");
            },
            failure: function (response){
                alert("Hubo un error");
            }
        });
    });
});