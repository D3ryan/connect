jQuery.validator.methods.email = function( value, element ) {
  return this.optional(element) || /^([a-zA-Z0-9_]+(?:[.-]?[a-zA-Z0-9]+)*@[a-zA-Z0-9]+(?:[.-]?[a-zA-Z0-9]+)*\.[a-zA-Z]{2,7})$/.test(value);
}

jQuery.validator.addMethod("names", function(value, element) {
    return this.optional(element) || /^[A-zÁ-ÿ]+([-\s]{1}[A-zÁ-ÿ]+)*$/i.test(value);
  }, "Letters only please");

jQuery.validator.addMethod("exactlength", function(value, element, param) {
return this.optional(element) || value.length == param;
}, $.validator.format("Please enter exactly {0} characters."));

jQuery.validator.addMethod("completename", function(value, element) {
    return this.optional(element) || /^[A-zÁ-ÿ]+([-\s]{1}[A-zÁ-ÿ]+)*([-\s]{1}[A-zÁ-ÿ]+)*([-\s]{1}[A-zÁ-ÿ]+)*$/i.test(value);
  }, "Letters only please");

$(document).ready(function(){
    /* ------------------- Login ------------------ */
    // $("#btn_login").click(function() {
        $("#LoginForm").validate({
          rules: {
            email: {
              required: true,
              email: true
            },
            password:{
              required: true,
            }
          },
          messages: {
            email:{
              required: "Por favor ingrese un correo.",
              email: "Por favor ingrese un formato correcto."
            },
            password:{
              required: "Por favor ingrese su contraseña."
            }
          }
        });
    // });

    /* ------------------- Signup ------------------ */
    // $("#btn_signup").click(function() {
        $("#signupForm").validate({		
        errorElement: "p",
        rules: {
            name: {
                required: true,
                names: true
            },
            lastname: {
                required: true,
                names: true
            },
            email: {
                required: true,
                email: true,
                remote: {
                    url: "checkEmailSignup",
                    type: "POST"
                }
            },
            password: {
                required: true
            },
            usertype: {
                required: true
            },
            plantype: {
                required: true
            },
            country: {
                required: true
            },
            cardnumber: {
                required: true,
                number: true,
                exactlength: 16
            },
            securitycode: {
                required: true,
                exactlength: 4,
                number: true
            },
            month: {
                required: true
            },
            year: {
                required: true
            },
            cardholder: {
                required: true,
                completename: true
            }
        },
        messages: {
            name: {
                required: "Por favor ingrese su nombre(s).",
                names: "Por favor ingrese bien su nombre(s)."
            },
            lastname: {
                required: "Por favor ingrese su apellido(s).",
                names: "Por favor ingrese bien su apellido(s)."
            },
            email:{
                required: "Por favor ingrese un correo.",
                email: "Por favor ingrese un formato correcto.",
                remote: "Ya hay un usuario con ese correo"
            },
            password: {
                required: "Por favor ingrese su contraseña."
            },
            usertype: {
                required: "Por favor elija un tipo de licencia."
            },
            plantype: {
                required: "Por favor elija un tipo de plan."
            },
            country: {
                required: "Por favor elija un país."
            },
            cardnumber: {
                required: "Por favor ingrese su tarjeta",
                number: "Por favor ingrese correctamente su tarjeta",
                exactlength: "Por favor ingrese los {0} numeros de su tarjeta"
            },
            securitycode: {
                required: "Por favor ingrese el código de seguridad de su tarjeta",
                exactlength: "Por favor ingrese los {0} números de atras de su tarjeta",
                number: "Por favor ingrese solo los números de su tarjeta"
            },
            month: {
                required: "Por favor ingrese un mes"
            },
            year: {
                required: "Por favor ingrese un año"
            },
            cardholder: {
                required: "Por favor ingrese el titular",
                completename: "Por favor ingrese el nombre del titular correctamente"
            }
        },

        errorPlacement:  function(error, element) {
            if (element.attr("name") == "usertype")
            {
                error.insertAfter("#usertype-container");
            }
            else{
                error.insertAfter(element);
            }
        }
        
        });
    // });

    /* ------------------- Update ------------------ */
    // $("#btn_updateSecure").click(function() {
     	$("#updateForm").validate({
        // invalidHandler: function(event, validator) {
        //     $("#exampleModal1").modal("hide");
        // },
        errorElement: "p",
        rules: {
            name: {
                required: true,
                names: true
            },
            lastname: {
                required: true,
                names: true
            },
            password: {
                required: true
            },
            plantype: {
                required: true
            },
            country: {
                required: true
            },
            cardnumber: {
                required: true,
                number: true,
                exactlength: 16
            },
            securitycode: {
                required: true,
                exactlength: 4,
                number: true
            },
            month: {
                required: true
            },
            year: {
                required: true
            },
            cardholder: {
                required: true,
                completename: true
            }
        },
        messages: {
            name: {
                required: "Por favor ingrese su nombre(s).",
                names: "Por favor ingrese bien su nombre(s)."
            },
            lastname: {
                required: "Por favor ingrese su apellido(s).",
                names: "Por favor ingrese bien su apellido(s)."
            },
            password: {
                required: "Por favor ingrese su contraseña."
            },
            plantype: {
                required: "Por favor elija un tipo de plan."
            },
            country: {
                required: "Por favor elija un país."
            },
            cardnumber: {
                required: "Por favor ingrese su tarjeta",
                number: "Por favor ingrese correctamente su tarjeta",
                exactlength: "Por favor ingrese los {0} numeros de su tarjeta"
            },
            securitycode: {
                required: "Por favor ingrese el código de seguridad de su tarjeta",
                exactlength: "Por favor ingrese los {0} números de atras de su tarjeta",
                number: "Por favor ingrese solo los números de su tarjeta"
            },
            month: {
                required: "Por favor ingrese un mes"
            },
            year: {
                required: "Por favor ingrese un año"
            },
            cardholder: {
                required: "Por favor ingrese el titular",
                completename: "Por favor ingrese el nombre del titular correctamente"
            }
        },
        
        });
    // });

     /* ------------------- Activate ------------------ */
    //  $("#btn_activate").click(function() {
        $("#activateForm").validate({
       errorElement: "p",
       rules: {
           name: {
               required: true,
               names: true
           },
           lastname: {
               required: true,
               names: true
           },
           password: {
               required: true
           },
           plantype: {
               required: true
           },
           country: {
               required: true
           },
           cardnumber: {
               required: true,
               number: true,
               exactlength: 16
           },
           securitycode: {
               required: true,
               exactlength: 4,
               number: true
           },
           month: {
               required: true
           },
           year: {
               required: true
           },
           cardholder: {
               required: true,
               completename: true
           }
       },
       messages: {
           name: {
               required: "Por favor ingrese su nombre(s).",
               names: "Por favor ingrese bien su nombre(s)."
           },
           lastname: {
               required: "Por favor ingrese su apellido(s).",
               names: "Por favor ingrese bien su apellido(s)."
           },
           password: {
               required: "Por favor ingrese su contraseña."
           },
           plantype: {
               required: "Por favor elija un tipo de plan."
           },
           country: {
               required: "Por favor elija un país."
           },
           cardnumber: {
               required: "Por favor ingrese su tarjeta",
               number: "Por favor ingrese correctamente su tarjeta",
               exactlength: "Por favor ingrese los {0} numeros de su tarjeta"
           },
           securitycode: {
               required: "Por favor ingrese el código de seguridad de su tarjeta",
               exactlength: "Por favor ingrese los {0} números de atras de su tarjeta",
               number: "Por favor ingrese solo los números de su tarjeta"
           },
           month: {
               required: "Por favor ingrese un mes"
           },
           year: {
               required: "Por favor ingrese un año"
           },
           cardholder: {
               required: "Por favor ingrese el titular",
               completename: "Por favor ingrese el nombre del titular correctamente"
           }
       },
       
       });
//    });

/* ------------------- Support ------------------ */
        $("#supportForm").validate({
            submitHandler: function (form) {  
                var subjectInput = $("#floatingInput").val();
                var commentInput = $("#floatingTextarea").val();
                $.ajax({
                    type: "post",
                    url: "DashHelp",
                    data: {
                        subject: subjectInput,
                        comment: commentInput
                    },
                    success: function (response) {
                        $.sweetModal({
                            content: '¡Recibimos tu ticket, pronto te contactaremos!.',
                            icon: $.sweetModal.ICON_SUCCESS,
                            theme: $.sweetModal.THEME_DARK
                        });

                        $("#floatingInput").val("");
                        $("#floatingTextarea").val("");
                        $("#loading-container").removeClass("d-flex");
                        $("#loading-container").addClass("d-none");
                    },
                    error: function (response) {   
                        $.sweetModal({
                            content: 'Hubo un error, intentalo mas tarde.',
                            icon: $.sweetModal.ICON_ERROR,
                            theme: $.sweetModal.THEME_DARK
                        });
                        $("#floatingInput").val("");
                        $("#floatingTextarea").val("");
                        $("#loading-container").removeClass("d-flex");
                        $("#loading-container").addClass("d-none");
                    }
                    
                });
                
            },
            errorElement: "p",
            rules: {
                subject: {
                    required: true
                },
                comment: {
                    required: true
                }
            },
            messages: {
                subject: {
                    required: "Por favor ingrese algún asunto"
                },
                comment: {
                    required: "Por favor ingrese algún comentario"
                }
            }
        });

        /* ------------------- Contact ------------------ */
        $("#contactForm").validate({
            submitHandler: function (form) {  
                var emailInput = $("#floatingEmail").val();
                var subjectInput = $("#floatingSubject").val();
                var commentInput = $("#floatingTextarea").val();
                $.ajax({
                    type: "post",
                    url: "DashContact",
                    data: {
                        email: emailInput,
                        subject: subjectInput,
                        comment: commentInput
                    },
                    success: function (response) {
                        $.sweetModal({
                            content: '¡Tu email se ha enviado con exito!.',
                            icon: $.sweetModal.ICON_SUCCESS,
                            theme: $.sweetModal.THEME_DARK
                        });

                        $("#floatingEmail").val("");
                        $("#floatingSubject").val("");
                        $("#floatingTextarea").val("");
                        $("#loading-container").removeClass("d-flex");
                        $("#loading-container").addClass("d-none");
                    },
                    error: function (response) {   
                        $.sweetModal({
                            content: 'Hubo un error, intentalo mas tarde.',
                            icon: $.sweetModal.ICON_ERROR,
                            theme: $.sweetModal.THEME_DARK
                        });
                        $("#floatingEmail").val("");
                        $("#floatingSubject").val("");
                        $("#floatingTextarea").val("");
                        $("#loading-container").removeClass("d-flex");
                        $("#loading-container").addClass("d-none");
                    }
                    
                });
                
            },
            errorElement: "p",
            errorClass: "error-contact",
            rules: {
                email: {
                    required: true,
                    email: true
                },
                subject: {
                    required: true
                },
                comment: {
                    required: true
                }
            },
            messages: {
                email:{
                    required: "Por favor ingrese un correo.",
                    email: "Por favor ingrese un formato correcto."
                },
                subject: {
                    required: "Por favor ingrese algún asunto"
                },
                comment: {
                    required: "Por favor ingrese algún comentario"
                }
            }
        });

        $("#btn-contact").click(function () { 
            $("#loading-container").removeClass("d-none");
            $("#loading-container").addClass("d-flex");
        });

        $("#btn-help").click(function () { 
            $("#loading-container").removeClass("d-none");
            $("#loading-container").addClass("d-flex");
        });
});