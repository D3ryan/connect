package com.Connect.dao;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;

import com.Connect.models.Users_Model;
import com.Connect.utils.DbConnection;

public class UsersDAO {
	
	public static int insertUpdateDeleteUsers(String pOpc, Users_Model user) throws Exception {
    	Connection con = null;
    	CallableStatement statement = null;

        int rowsAffectted = 0;
        try {
            con = DbConnection.getConnection();
            
            statement = con.prepareCall("CALL sp_Users(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
           
            statement.setString(1, pOpc);            
            statement.setLong(2, user.getId_User());            
            statement.setInt(3, user.getId_Plan());
            statement.setString(4, user.getFirstNameUser());
            statement.setString(5, user.getLastNameUser());
            statement.setString(6, user.getEmailUser());
            statement.setString(7, user.getPasswordUser());
            statement.setString(8, user.getLicenseTypeUser());
            statement.setString(9, user.getCountryUser());
            statement.setString(10, user.getCardNumberUser());
            statement.setInt(11, user.getMonthUser());
            statement.setInt(12, user.getYearUser());
            statement.setString(13, user.getSecurityNumberUser());
            statement.setString(14, user.getCardholderUser());
            statement.setBoolean(15, user.isStatusUser());
         
            rowsAffectted = statement.executeUpdate();
            
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            rowsAffectted = 0;
        } finally {
        	statement.close();
            con.close();
        }
        return rowsAffectted;
    }
	
	public static List<Users_Model> getUsers(String pOpc, Users_Model user) throws Exception  {
        List<Users_Model> UsersList = new ArrayList<>();
        Connection con = null;
        CallableStatement statement = null;

        try {
            con = DbConnection.getConnection();
            statement = con.prepareCall("CALL sp_Users(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
            
            statement.setString(1, pOpc);            
            statement.setLong(2, user.getId_User());            
            statement.setInt(3, user.getId_Plan());
            statement.setString(4, user.getFirstNameUser());
            statement.setString(5, user.getLastNameUser());
            statement.setString(6, user.getEmailUser());
            statement.setString(7, user.getPasswordUser());
            statement.setString(8, user.getLicenseTypeUser());
            statement.setString(9, user.getCountryUser());
            statement.setString(10, user.getCardNumberUser());
            statement.setInt(11, user.getMonthUser());
            statement.setInt(12, user.getYearUser());
            statement.setString(13, user.getSecurityNumberUser());
            statement.setString(14, user.getCardholderUser());
            statement.setBoolean(15, user.isStatusUser());
            
            ResultSet resultSet = statement.executeQuery();
            
            while (resultSet.next()) {
                
                long Id_User = resultSet.getLong("Id_User");
                int Id_Plan = resultSet.getInt("Id_Plan");
                String FirstNameUser = resultSet.getString("FirstName_User");
                String LastNameUser = resultSet.getString("LastName_User");
                String EmailUser = resultSet.getString("Email_User");
                String PasswordUser = resultSet.getString("Password_User");
                String LicenseTypeUser = resultSet.getString("LicenseType_User");
                String CountryUser = resultSet.getString("Country_User");
                String CardNumberUser = resultSet.getString("CardNumber_User");
                int MonthUser = resultSet.getInt("Month_User");
                int YearUser = resultSet.getInt("Year_User");
                String SecurityNumberUser = resultSet.getString("SecurityNumber_User");
                String CardholderUser = resultSet.getString("Cardholder_User");
                boolean StatusUser = resultSet.getBoolean("Status_User");
                
                String NamePlan = "";
                String TypePlan = "";
                BigDecimal PricePlan = new BigDecimal(0);
                boolean StatusPlan = true;
                long Id_Payment = 0;
                Date CutoffDatePayment = null;
                
                if(DbConnection.hasColumn(resultSet, "Name_Plan"))
                	NamePlan = resultSet.getString("Name_Plan");
                
                if(DbConnection.hasColumn(resultSet, "Type_Plan"))
                	TypePlan = resultSet.getString("Type_Plan");
                
                if(DbConnection.hasColumn(resultSet, "Price_Plan"))
                	PricePlan = resultSet.getBigDecimal("Price_Plan");
                
                if(DbConnection.hasColumn(resultSet, "Status_Plan"))
                	StatusPlan = resultSet.getBoolean("Status_Plan");
                
                if(DbConnection.hasColumn(resultSet, "Status_Plan"))
                	StatusPlan = resultSet.getBoolean("Status_Plan");
                
                if(DbConnection.hasColumn(resultSet, "Id_Payment"))
                	Id_Payment = resultSet.getLong("Id_Payment");
                
                if(DbConnection.hasColumn(resultSet, "CutoffDate_Payment"))
                	CutoffDatePayment = resultSet.getDate("CutoffDate_Payment");
                	

                UsersList.add(new Users_Model(
                		Id_User, FirstNameUser, LastNameUser,
                		EmailUser, PasswordUser, LicenseTypeUser, CountryUser, CardNumberUser,
                		MonthUser, YearUser, SecurityNumberUser, CardholderUser, StatusUser, Id_Plan, 
                		NamePlan, TypePlan, PricePlan, StatusPlan, Id_Payment, CutoffDatePayment
                		));
                
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            statement.close();
            con.close();
        }

        return UsersList;
    }
	
	
}
