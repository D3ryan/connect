package com.Connect.dao;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.Connect.models.Plans_Model;
import com.Connect.utils.DbConnection;

public class PlansDAO {

	public static int insertUpdateDeletePlans(String pOpc, Plans_Model plan) throws Exception {
		
    	Connection con = null;
    	CallableStatement statement = null;

        int rowsAffectted = 0;
        try {
            con = DbConnection.getConnection();
            
            statement = con.prepareCall("CALL sp_plans( ?, ?, ?, ?, ?, ?);");
           
            statement.setString(1, pOpc);      
            statement.setInt(2, plan.getId_Plan());            
            statement.setString(3, plan.getNamePlan());            
            statement.setString(4, plan.getTypePlan());
            statement.setBigDecimal(5, plan.getPricePlan());
            statement.setBoolean(6, plan.isStatusPlan()); 
            
            rowsAffectted = statement.executeUpdate();
            
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            rowsAffectted = 0;
        } finally {
        	statement.close();
            con.close();
        }
        return rowsAffectted;
    }
	
	public static List<Plans_Model> getPlans(String pOpc, Plans_Model plan) throws Exception  {
        List<Plans_Model> PlanList = new ArrayList<>();
        Connection con = null;
        CallableStatement statement = null;

        try {
            con = DbConnection.getConnection();
            statement = con.prepareCall("CALL sp_plans( ?, ?, ?, ?, ?, ?);");
                 
            statement.setString(1, pOpc);      
            statement.setInt(2, plan.getId_Plan());            
            statement.setString(3, plan.getNamePlan());            
            statement.setString(4, plan.getTypePlan());
            statement.setBigDecimal(5, plan.getPricePlan());
            statement.setBoolean(6, plan.isStatusPlan()); 
            
            ResultSet resultSet = statement.executeQuery();
            
            while (resultSet.next()) {
            	
            	int Id_Plan = resultSet.getInt("Id_Plan");
                String NamePlan = resultSet.getString("Name_Plan");
                String TypePlan	= resultSet.getString("Type_Plan");
                BigDecimal PricePlan = resultSet.getBigDecimal("Price_Plan");
                boolean StatusPlan = resultSet.getBoolean("Status_Plan");
                
                int Better_Plan = 0;
                
                if(DbConnection.hasColumn(resultSet, "Better_Plan"))
                	Better_Plan = resultSet.getInt("Better_Plan");
                
                PlanList.add(new Plans_Model( Id_Plan, NamePlan, TypePlan, PricePlan, StatusPlan, Better_Plan));
                
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            statement.close();
            con.close();
        }

        return PlanList;
    }
	
}

