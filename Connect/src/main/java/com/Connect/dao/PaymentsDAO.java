package com.Connect.dao;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.Connect.models.Payments_Model;
import com.Connect.utils.DbConnection;

public class PaymentsDAO {
	
	public static int insertUpdateDeletePayments(String pOpc, Payments_Model payment) throws Exception {
		
    	Connection con = null;
    	CallableStatement statement = null;

        int rowsAffectted = 0;
        try {
            con = DbConnection.getConnection();
            
            statement = con.prepareCall("CALL sp_Payments( ?, ?, ?, ?, ?, ?, ?, ?);");
           
            statement.setString(1, pOpc);      
            statement.setLong(2, payment.getId_Payment());            
            statement.setLong(3, payment.getId_User());            
            statement.setLong(4, payment.getId_Plan());
            
            if (payment.getBeginDatePayment() != null) {
            	java.util.Date utilBeginDatePayment = payment.getBeginDatePayment();
        		java.sql.Date sqlBeginDatePayment = new java.sql.Date(utilBeginDatePayment.getTime());    		
                statement.setDate(5, sqlBeginDatePayment);
            }
            else {
            	statement.setDate(5, null);
            }
            
            if (payment.getCutoffDatePayment() != null) {
            	java.util.Date utilCutoffDatePayment = payment.getCutoffDatePayment();
        		java.sql.Date sqlCutoffDatePayment = new java.sql.Date(utilCutoffDatePayment.getTime());    		
                statement.setDate(6, sqlCutoffDatePayment);
            }
            else {
            	statement.setDate(6, null);
            }
            
            statement.setBigDecimal(7, payment.getAmountPayment());    
            statement.setBoolean(8, payment.getStatusPayment());    
            
            rowsAffectted = statement.executeUpdate();
            
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            rowsAffectted = 0;
        } finally {
        	statement.close();
            con.close();
        }
        return rowsAffectted;
    }
	
	public static List<Payments_Model> getPayments(String pOpc, Payments_Model payment) throws Exception  {
        List<Payments_Model> PaymentList = new ArrayList<>();
        Connection con = null;
        CallableStatement statement = null;

        try {
            con = DbConnection.getConnection();
            statement = con.prepareCall("CALL sp_Payments( ?, ?, ?, ?, ?, ?, ?, ?);");
            
            statement.setString(1, pOpc);      
            statement.setLong(2, payment.getId_Payment());            
            statement.setLong(3, payment.getId_User());            
            statement.setLong(4, payment.getId_Plan());
            
            if (payment.getBeginDatePayment() != null) {
            	java.util.Date utilBeginDatePayment = payment.getBeginDatePayment();
        		java.sql.Date sqlBeginDatePayment = new java.sql.Date(utilBeginDatePayment.getTime());    		
                statement.setDate(5, sqlBeginDatePayment);
            }
            else {
            	statement.setDate(5, null);
            }
            
            if (payment.getCutoffDatePayment() != null) {
            	java.util.Date utilCutoffDatePayment = payment.getCutoffDatePayment();
        		java.sql.Date sqlCutoffDatePayment = new java.sql.Date(utilCutoffDatePayment.getTime());    		
                statement.setDate(6, sqlCutoffDatePayment);
            }
            else {
            	statement.setDate(6, null);
            }
            
            statement.setBigDecimal(7, payment.getAmountPayment());
            statement.setBoolean(8, payment.getStatusPayment()); 
            
            ResultSet resultSet = statement.executeQuery();
            
            while (resultSet.next()) {
                
                long Id_Payment = resultSet.getLong("Id_Payment");
                long Id_User = resultSet.getLong("Id_User");
                int Id_Plan = resultSet.getInt("Id_Plan");
                Date BeginDate = resultSet.getDate("BeginDate_Payment");
                Date CutoffDate = resultSet.getDate("CutoffDate_Payment");
                BigDecimal AmountPayment = resultSet.getBigDecimal("Amount_Payment");
                boolean StatusPayment = resultSet.getBoolean("Status_Payment");
                System.out.println(StatusPayment);
                String NamePlan = "";
                String TypePlan = "";
                BigDecimal PricePlan = null;
                
                if(DbConnection.hasColumn(resultSet, "Name_Plan"))
                	NamePlan = resultSet.getString("Name_Plan");
                
                if(DbConnection.hasColumn(resultSet, "Type_Plan"))
                	TypePlan = resultSet.getString("Type_Plan");
                
                if(DbConnection.hasColumn(resultSet, "Price_Plan"))
                	PricePlan = resultSet.getBigDecimal("Price_Plan");
                
                	
                
                PaymentList.add(new Payments_Model( Id_Payment, Id_User, Id_Plan, NamePlan, TypePlan, PricePlan, BeginDate, CutoffDate, AmountPayment, StatusPayment));
                
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            statement.close();
            con.close();
        }

        return PaymentList;
    }
}	
