package com.Connect.models;

import java.math.BigDecimal;
import java.util.Date;

public class Users_Model {
	private long Id_User;
	private int Id_Plan;
	private String FirstNameUser;
	private String LastNameUser;
	private String EmailUser;
	private String PasswordUser;
	private String LicenseTypeUser;
	private String CountryUser;
	private String CardNumberUser;
	private int MonthUser;
	private int YearUser;
	private String SecurityNumberUser;
	private String CardholderUser;
	private boolean StatusUser;
	
	
	private String NamePlan;
	private String TypePlan;
	private BigDecimal PricePlan;
	private boolean StatusPlan;
	
	private long Id_Payment;
	private Date CutoffDate;
	
	
	public Users_Model() {
		super();
	}
	
	public Users_Model(long id_User) {
		super();
		Id_User = id_User;
		
	}
	
	public Users_Model(long id_User, String firstNameUser, String lastNameUser, String emailUser,
			   String passwordUser, String licenseTypeUser, String countryUser, String cardNumberUser,
			   int monthUser, int yearUser, String securityNumberUser, String cardholderUser, boolean statusUser,
			   int id_Plan, String namePlan, String typePlan, BigDecimal pricePlan, boolean statusPlan, long id_Payment, Date cutoffDate) {
		super();
		Id_User = id_User;
		Id_Plan = id_Plan;
		FirstNameUser = firstNameUser;
		LastNameUser = lastNameUser;
		EmailUser = emailUser;
		PasswordUser = passwordUser;
		LicenseTypeUser = licenseTypeUser;
		CountryUser = countryUser;
		CardNumberUser = cardNumberUser;
		MonthUser = monthUser;
		YearUser = yearUser;
		SecurityNumberUser = securityNumberUser;
		CardholderUser = cardholderUser;
		StatusUser = statusUser;
		NamePlan = namePlan;
		TypePlan = typePlan;
		PricePlan = pricePlan;
		StatusPlan = statusPlan;
		Id_Payment = id_Payment;
		CutoffDate = cutoffDate;
	}

	public Users_Model(long id_User, int id_Plan, String firstNameUser, String lastNameUser, String emailUser,
					   String passwordUser, String licenseTypeUser, String countryUser, String cardNumberUser,
					   int monthUser, int yearUser, String securityNumberUser, String cardholderUser,
					   boolean statusUser) {
		super();
		Id_User = id_User;
		Id_Plan = id_Plan;
		FirstNameUser = firstNameUser;
		LastNameUser = lastNameUser;
		EmailUser = emailUser;
		PasswordUser = passwordUser;
		LicenseTypeUser = licenseTypeUser;
		CountryUser = countryUser;
		CardNumberUser = cardNumberUser;
		MonthUser = monthUser;
		YearUser = yearUser;
		SecurityNumberUser = securityNumberUser;
		CardholderUser = cardholderUser;
		StatusUser = statusUser;
		
	}
	
	
	public Users_Model(String emailUser, String passwordUser) {
		
		super();
		EmailUser = emailUser;
		PasswordUser = passwordUser;
		
	}
	
	public Users_Model(int id_Plan, String firstNameUser, String lastNameUser, String emailUser,
			   String passwordUser, String licenseTypeUser, String countryUser, String cardNumberUser,
			   int monthUser, int yearUser, String securityNumberUser, String cardholderUser) {
		
		super();
		Id_Plan = id_Plan;
		FirstNameUser = firstNameUser;
		LastNameUser = lastNameUser;
		EmailUser = emailUser;
		PasswordUser = passwordUser;
		LicenseTypeUser = licenseTypeUser;
		CountryUser = countryUser;
		CardNumberUser = cardNumberUser;
		MonthUser = monthUser;
		YearUser = yearUser;
		SecurityNumberUser = securityNumberUser;
		CardholderUser = cardholderUser;
		
	}
	
	public Users_Model(int id_User, boolean statusUser) {		
		super();
		Id_User = id_User;
		StatusUser = statusUser;
	}
	
	public Users_Model(long id_User, String firstNameUser, String lastNameUser, String emailUser,
			   String passwordUser, String licenseTypeUser, String countryUser, String cardNumberUser,
			   int monthUser, int yearUser, String securityNumberUser, String cardholderUser, boolean statusUser,
			   int id_Plan, String namePlan, String typePlan, BigDecimal pricePlan, boolean statusPlan ) {
		
		super();
		Id_User = id_User;
		FirstNameUser = firstNameUser;
		LastNameUser = lastNameUser;
		EmailUser = emailUser;
		PasswordUser = passwordUser;
		LicenseTypeUser = licenseTypeUser;
		CountryUser = countryUser;
		CardNumberUser = cardNumberUser;
		MonthUser = monthUser;
		YearUser = yearUser;
		SecurityNumberUser = securityNumberUser;
		CardholderUser = cardholderUser;
		StatusUser = statusUser;
		
		Id_Plan = id_Plan;
		NamePlan = namePlan;
		TypePlan = typePlan;
		PricePlan = pricePlan;
		StatusPlan = statusPlan;
		
	}

	//
	
	public Users_Model(long id_User, int id_Plan, String firstNameUser, String lastNameUser, String passwordUser, String countryUser,
			String cardNumberUser, int monthUser, int yearUser, String securityNumberUser, String cardholderUser) {
		
		Id_User = id_User;
		Id_Plan = id_Plan;
		FirstNameUser = firstNameUser;
		LastNameUser = lastNameUser;
		PasswordUser = passwordUser;
		CountryUser = countryUser;
		CardNumberUser = cardNumberUser;
		MonthUser = monthUser;
		YearUser = yearUser;
		SecurityNumberUser = securityNumberUser;
		CardholderUser = cardholderUser;
	}

	public Users_Model(long id_User, boolean statusUser) {
		Id_User = id_User;
		StatusUser = statusUser;
	}

	public Users_Model(String emailUser) {
		EmailUser = emailUser;
	}

	public long getId_User() {
		return Id_User;
	}

	public void setId_User(long id_User) {
		Id_User = id_User;
	}

	public int getId_Plan() {
		return Id_Plan;
	}

	public void setId_Plan(int id_Plan) {
		Id_Plan = id_Plan;
	}

	public String getFirstNameUser() {
		return FirstNameUser;
	}

	public void setFirstNameUser(String firstNameUser) {
		FirstNameUser = firstNameUser;
	}

	public String getLastNameUser() {
		return LastNameUser;
	}

	public void setLastNameUser(String lastNameUser) {
		LastNameUser = lastNameUser;
	}

	public String getEmailUser() {
		return EmailUser;
	}

	public void setEmailUser(String emailUser) {
		EmailUser = emailUser;
	}

	public String getPasswordUser() {
		return PasswordUser;
	}

	public void setPasswordUser(String passwordUser) {
		PasswordUser = passwordUser;
	}

	public String getLicenseTypeUser() {
		return LicenseTypeUser;
	}

	public void setLicenseTypeUser(String licenseTypeUser) {
		LicenseTypeUser = licenseTypeUser;
	}

	public String getCountryUser() {
		return CountryUser;
	}

	public void setCountryUser(String countryUser) {
		CountryUser = countryUser;
	}

	public String getCardNumberUser() {
		return CardNumberUser;
	}

	public void setCardNumberUser(String cardNumberUser) {
		CardNumberUser = cardNumberUser;
	}

	public int getMonthUser() {
		return MonthUser;
	}

	public void setMonthUser(int monthUser) {
		MonthUser = monthUser;
	}

	public int getYearUser() {
		return YearUser;
	}

	public void setYearUser(int yearUser) {
		YearUser = yearUser;
	}

	public String getSecurityNumberUser() {
		return SecurityNumberUser;
	}

	public void setSecurityNumberUser(String securityNumberUser) {
		SecurityNumberUser = securityNumberUser;
	}

	public String getCardholderUser() {
		return CardholderUser;
	}

	public void setCardholderUser(String cardholderUser) {
		CardholderUser = cardholderUser;
	}

	public boolean isStatusUser() {
		return StatusUser;
	}

	public void setStatusUser(boolean statusUser) {
		StatusUser = statusUser;
	}

	public String getNamePlan() {
		return NamePlan;
	}

	public void setNamePlan(String namePlan) {
		NamePlan = namePlan;
	}

	public String getTypePlan() {
		return TypePlan;
	}

	public void setTypePlan(String typePlan) {
		TypePlan = typePlan;
	}

	public BigDecimal getPricePlan() {
		return PricePlan;
	}

	public void setPricePlan(BigDecimal pricePlan) {
		PricePlan = pricePlan;
	}

	public boolean isStatusPlan() {
		return StatusPlan;
	}

	public void setStatusPlan(boolean statusPlan) {
		StatusPlan = statusPlan;
	}

	public long getId_Payment() {
		return Id_Payment;
	}

	public void setId_Payment(long id_Payment) {
		Id_Payment = id_Payment;
	}

	public Date getCutoffDate() {
		return CutoffDate;
	}

	public void setCutoffDate(Date cutoffDate) {
		CutoffDate = cutoffDate;
	}
	
	public String getCutoffDateToString() {
		return  CutoffDate.toString();
	}
	
}

