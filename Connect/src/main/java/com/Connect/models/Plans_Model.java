package com.Connect.models;

import java.math.BigDecimal;

public class Plans_Model {
	
	private int Id_Plan;
	private String NamePlan;
	private String TypePlan;
	private BigDecimal PricePlan;
	private boolean statusPlan;
	
	private int Better_Plan;
	
	public Plans_Model() {
		super();
	}
	
	public Plans_Model(int id_Plan) {
		super();
		Id_Plan = id_Plan;
	}

	public Plans_Model(int id_Plan, String namePlan, String typePlan, BigDecimal pricePlan, boolean statusPlan) {
		super();
		Id_Plan = id_Plan;
		NamePlan = namePlan;
		TypePlan = typePlan;
		PricePlan = pricePlan;
		this.statusPlan = statusPlan;
	}
	
	public Plans_Model(int id_Plan, String namePlan, String typePlan, BigDecimal pricePlan, boolean statusPlan, int better_Plan) {
		super();
		Id_Plan = id_Plan;
		NamePlan = namePlan;
		TypePlan = typePlan;
		PricePlan = pricePlan;
		Better_Plan = better_Plan;
	}
	
	public Plans_Model(String namePlan, String typePlan, BigDecimal pricePlan, boolean statusPlan) {
		super();
		NamePlan = namePlan;
		TypePlan = typePlan;
		PricePlan = pricePlan;
		this.statusPlan = statusPlan;
	}

	public Plans_Model(String namePlan, String typePlan, BigDecimal pricePlan) {
		super();
		NamePlan = namePlan;
		TypePlan = typePlan;
		PricePlan = pricePlan;
	}

	public int getId_Plan() {
		return Id_Plan;
	}

	public void setId_Plan(int id_Plan) {
		Id_Plan = id_Plan;
	}

	public String getNamePlan() {
		return NamePlan;
	}

	public void setNamePlan(String namePlan) {
		NamePlan = namePlan;
	}

	public String getTypePlan() {
		return TypePlan;
	}

	public void setTypePlan(String typePlan) {
		TypePlan = typePlan;
	}

	public BigDecimal getPricePlan() {
		return PricePlan;
	}

	public void setPricePlan(BigDecimal pricePlan) {
		PricePlan = pricePlan;
	}

	public boolean isStatusPlan() {
		return statusPlan;
	}

	public void setStatusPlan(boolean statusPlan) {
		this.statusPlan = statusPlan;
	}

	public int getBetter_Plan() {
		return Better_Plan;
	}

	public void setBetter_Plan(int better_Plan) {
		Better_Plan = better_Plan;
	}
	
	
	
	
	

}
