package com.Connect.models;

import java.math.BigDecimal;
import java.util.Date;

public class Payments_Model {
	private long Id_Payment;
	private long Id_User;
	private int Id_Plan;
	private String NamePlan;
	private String TypePlan;
	private BigDecimal PricePlan;
	private Date BeginDatePayment;
	private Date CutoffDatePayment;
	private BigDecimal AmountPayment;
	private boolean StatusPayment;
	
	
	public Payments_Model(long id_Payment, long id_User, int id_Plan, Date beginDatePayment, Date cutoffDatePayment,
			BigDecimal amountPayment) {
		super();
		Id_Payment = id_Payment;
		Id_User = id_User;
		Id_Plan = id_Plan;
		BeginDatePayment = beginDatePayment;
		CutoffDatePayment = cutoffDatePayment;
		AmountPayment = amountPayment;
	}


	public Payments_Model(long id_User, int id_Plan, Date beginDatePayment, Date cutoffDatePayment,
			BigDecimal amountPayment) {
		super();
		Id_User = id_User;
		Id_Plan = id_Plan;
		BeginDatePayment = beginDatePayment;
		CutoffDatePayment = cutoffDatePayment;
		AmountPayment = amountPayment;
	}
	
	public Payments_Model(long id_User, int id_Plan, Date beginDatePayment, Date cutoffDatePayment,
			BigDecimal amountPayment, boolean statusPayment) {
		super();
		Id_User = id_User;
		Id_Plan = id_Plan;
		BeginDatePayment = beginDatePayment;
		CutoffDatePayment = cutoffDatePayment;
		AmountPayment = amountPayment;
		StatusPayment = statusPayment;
	}
	

	public Payments_Model(long id_Payment, long id_User, int id_Plan, String namePlan, String typePlan,
			Date beginDatePayment, Date cutoffDatePayment, BigDecimal amountPayment, boolean statusPayment) {
		super();
		Id_Payment = id_Payment;
		Id_User = id_User;
		Id_Plan = id_Plan;
		NamePlan = namePlan;
		TypePlan = typePlan;
		BeginDatePayment = beginDatePayment;
		CutoffDatePayment = cutoffDatePayment;
		AmountPayment = amountPayment;
		StatusPayment = statusPayment;
	}
	
	public Payments_Model(long id_Payment, long id_User, int id_Plan, String namePlan, String typePlan, BigDecimal pricePlan,
			Date beginDatePayment, Date cutoffDatePayment, BigDecimal amountPayment, boolean statusPayment) {
		super();
		Id_Payment = id_Payment;
		Id_User = id_User;
		Id_Plan = id_Plan;
		NamePlan = namePlan;
		TypePlan = typePlan;
		PricePlan = pricePlan;
		BeginDatePayment = beginDatePayment;
		CutoffDatePayment = cutoffDatePayment;
		AmountPayment = amountPayment;
		StatusPayment = statusPayment;
	}


	public Payments_Model(long id_User) {
		super();
		Id_User = id_User;
	}


	public Payments_Model(long id_User, Date beginDatePayment) {
		Id_User = id_User;
		BeginDatePayment = beginDatePayment;
	}


	public long getId_Payment() {
		return Id_Payment;
	}


	public void setId_Payment(long id_Payment) {
		Id_Payment = id_Payment;
	}


	public long getId_User() {
		return Id_User;
	}


	public void setId_User(long id_User) {
		Id_User = id_User;
	}


	public int getId_Plan() {
		return Id_Plan;
	}


	public void setId_Plan(int id_Plan) {
		Id_Plan = id_Plan;
	}


	public Date getBeginDatePayment() {
		return BeginDatePayment;
	}


	public void setBeginDatePayment(Date beginDatePayment) {
		BeginDatePayment = beginDatePayment;
	}


	public Date getCutoffDatePayment() {
		return CutoffDatePayment;
	}


	public void setCutoffDatePayment(Date cutoffDatePayment) {
		CutoffDatePayment = cutoffDatePayment;
	}


	public BigDecimal getAmountPayment() {
		return AmountPayment;
	}


	public void setAmountPayment(BigDecimal amountPayment) {
		AmountPayment = amountPayment;
	}


	public String getNamePlan() {
		return NamePlan;
	}


	public void setNamePlan(String namePlan) {
		NamePlan = namePlan;
	}

	public String getTypePlan() {
		return TypePlan;
	}

	public void setTypePlan(String typePlan) {
		TypePlan = typePlan;
	}


	public Boolean getStatusPayment() {
		return StatusPayment;
	}


	public void setStatusPayment(boolean statusPayment) {
		StatusPayment = statusPayment;
	}


	public BigDecimal getPricePlan() {
		return PricePlan;
	}


	public void setPricePlan(BigDecimal pricePlan) {
		PricePlan = pricePlan;
	}
	
	
}
