package com.Connect.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.Connect.dao.UsersDAO;
import com.Connect.models.Users_Model;

/**
 * Servlet implementation class checkEmailSignup
 */
@WebServlet("/checkEmailSignup")
public class checkEmailSignup extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public checkEmailSignup() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		 response.setCharacterEncoding("utf-8"); 
		checkUser(request, response);
		
	}
	
	protected void checkUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		List<Users_Model> usersList = null;
		Users_Model newUser = null;
		
		PrintWriter out= response.getWriter();
        response.setContentType("application/json");
        String json = "";
        
		
		String email = request.getParameter("email");
		
		newUser = new Users_Model(email);
		
		try {
			usersList = UsersDAO.getUsers("CE",newUser);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(!usersList.isEmpty())
			json += "false";
		else
			json += "true";
		System.out.println(json);
		out.print(json);
		
	}

}
