package com.Connect.controllers;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.Connect.dao.PaymentsDAO;
import com.Connect.dao.PlansDAO;
import com.Connect.dao.UsersDAO;
import com.Connect.models.Payments_Model;
import com.Connect.models.Plans_Model;
import com.Connect.models.Users_Model;

/**
 * Servlet implementation class DashAccount
 */
@WebServlet("/DashAccount")
public class DashAccount extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DashAccount() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("DoGet DashAccount");
		getPlans(request, response);
		getUser(request, response);
		getRecommendedPlan(request, response);
		RequestDispatcher dispatcher = request.getRequestDispatcher("dash-account.jsp");	
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		if(request.getParameter("logicChange") == null) {
			updateUser(request, response);
		}
		else if(request.getParameter("logicChange").equals("true")) {
			activateUser(request, response);
			updateInactiveUser(request, response);
			System.out.println(request.getParameter("logicChange true"));
		}
		else if(request.getParameter("logicChange").equals("false")) {
			deactivateUser(request, response);
			System.out.println(request.getParameter("logicChange false"));
		}

		response.sendRedirect("DashGeneral");
	}
	
	public void getUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Users_Model> selectedUser = null;
		Users_Model auxUser = null;
		
		long IdActiveUser = -1;
		
		if (request.getSession().getAttribute("ActiveUserId") != null) {
			IdActiveUser = (Long) request.getSession().getAttribute("ActiveUserId");
			System.out.println(IdActiveUser);
			auxUser = new Users_Model(IdActiveUser);
			
			try {
				selectedUser = UsersDAO.getUsers("S", auxUser);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		request.setAttribute("SelectedUser", selectedUser.get(0));
		request.setAttribute("ActiveUserId", IdActiveUser);
		
	}
	
	private void getPlans(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Plans_Model> planList = null;
        
        try {
        	planList = PlansDAO.getPlans("X", new Plans_Model());
		} catch (Exception e) {
			e.printStackTrace();
		}
       
        request.setAttribute("PlanList", planList);
	}
	
	private void updateUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		List<Users_Model> usersList = null;
		Users_Model UpdatedUser = null;
		
		usersList = getUserPlan(request, response);
		
		long Id_User = usersList.get(0).getId_User();
		String firstName = request.getParameter("name");
		String lastName = request.getParameter("lastname");
		String password = request.getParameter("password");
		int Id_Plan = Integer.parseInt(request.getParameter("plantype"));
		String country = request.getParameter("country");
		String cardnumber = request.getParameter("cardnumber");
		int month = Integer.parseInt(request.getParameter("month"));
		int year = Integer.parseInt(request.getParameter("year"));
		String securityCode = request.getParameter("securitycode");
		String cardholder = request.getParameter("cardholder");	
		
		UpdatedUser = new Users_Model(Id_User, Id_Plan, firstName, lastName, password, country,
				cardnumber, month, year, securityCode, cardholder
		);
		
		try {
			UsersDAO.insertUpdateDeleteUsers("DU", UpdatedUser);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(Id_Plan != usersList.get(0).getId_Plan())
			insertPayment(request, response);
		else
			System.out.println("Es el mismo plan o esta inactivo");
	}
	
	private void updateInactiveUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		List<Users_Model> usersList = null;
		Users_Model UpdatedUser = null;
		
		usersList = getUserPlan(request, response);
		
		long Id_User = usersList.get(0).getId_User();
		String firstName = request.getParameter("name");
		String lastName = request.getParameter("lastname");
		String password = request.getParameter("password");
		int Id_Plan = Integer.parseInt(request.getParameter("plantype"));
		String country = request.getParameter("country");
		String cardnumber = request.getParameter("cardnumber");
		int month = Integer.parseInt(request.getParameter("month"));
		int year = Integer.parseInt(request.getParameter("year"));
		String securityCode = request.getParameter("securitycode");
		String cardholder = request.getParameter("cardholder");	
		
		UpdatedUser = new Users_Model(Id_User, Id_Plan, firstName, lastName, password, country,
				cardnumber, month, year, securityCode, cardholder
		);
		
		try {
			UsersDAO.insertUpdateDeleteUsers("DU", UpdatedUser);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		insertPayment(request, response);
//		System.out.println("Es el mismo plan o esta inactivo");
	}

	private List<Users_Model> getUserPlan(HttpServletRequest request, HttpServletResponse response) {
		List<Users_Model> usersList = null;
		Users_Model selectedUser = null;
        
		if (request.getSession().getAttribute("ActiveUserId") != null) {
			long IdActiveUser = (Long) request.getSession().getAttribute("ActiveUserId");
			selectedUser = new Users_Model(IdActiveUser);

			try {
	        	usersList = UsersDAO.getUsers("CPU", selectedUser);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

        return usersList;
		
	}

	private void insertPayment(HttpServletRequest request, HttpServletResponse response) {
		List<Users_Model> usersList = null;
		Payments_Model newPayment = null;
		
		usersList = getUserPlan(request, response);
		
		long Id_User = usersList.get(0).getId_User();
		int Id_Plan = usersList.get(0).getId_Plan();
		System.out.println(Id_User);
		System.out.println(Id_Plan);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new java.util.Date());
		
		Date BeginDatePayment = calendar.getTime();
		Date CutoffDatePayment = null;
		
		switch (usersList.get(0).getTypePlan()) {
            case "Mensual":
    			calendar.add(Calendar.MONTH, 1);
    			CutoffDatePayment = calendar.getTime();
    			break;
            case "Anual":
            	calendar.add(Calendar.YEAR, 1);
            	CutoffDatePayment = calendar.getTime();
            	break;
            default: 
            	System.out.println("Tipo de plan mal definido en la bd");  
		
		}
		
		BigDecimal AmountPayment = new BigDecimal(0);
		if(usersList.get(0).getTypePlan().equals("Mensual")) {
			AmountPayment = usersList.get(0).getPricePlan();
		}
		else if(usersList.get(0).getTypePlan().equals("Anual")) {
			AmountPayment = usersList.get(0).getPricePlan().multiply(new BigDecimal(12));
		}
		
//		System.out.println(usersList.get(0).getPricePlan()); //Para checar como lo trae
		
		newPayment = new Payments_Model(Id_User, Id_Plan, BeginDatePayment, 
				CutoffDatePayment, AmountPayment, true);
		
		try {
			PaymentsDAO.insertUpdateDeletePayments("I", newPayment);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
//		System.out.println(BeginDatePayment);
//		System.out.println(CutoffDatePayment);
//		
			
		
	}
	
	private void activateUser(HttpServletRequest request, HttpServletResponse response) {
		Users_Model UpdatedUser = null;
		long IdActiveUser = -1;
		
		if (request.getSession().getAttribute("ActiveUserId") != null)
			IdActiveUser = (Long) request.getSession().getAttribute("ActiveUserId");
		
		UpdatedUser = new Users_Model(IdActiveUser, true);
		
		try {
			UsersDAO.insertUpdateDeleteUsers("LC", UpdatedUser);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	private void deactivateUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		Users_Model UpdatedUser = null;
		long IdActiveUser = -1;
		
		if (request.getSession().getAttribute("ActiveUserId") != null)
			IdActiveUser = (Long) request.getSession().getAttribute("ActiveUserId");
		
		UpdatedUser = new Users_Model(IdActiveUser, false);
		
		try {
			UsersDAO.insertUpdateDeleteUsers("LC", UpdatedUser);
			System.out.println("Logic");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	private void getRecommendedPlan(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		if(request.getParameter("plan") != null) {
			int Idplan = Integer.parseInt((String) request.getParameter("plan"));
			request.setAttribute("plan", Idplan);
			System.out.println(Idplan);
		}
	
	}

}
