package com.Connect.controllers;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.security.auth.message.callback.PrivateKeyCallback.Request;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.Connect.dao.UsersDAO;
import com.Connect.dao.PaymentsDAO;
import com.Connect.dao.PlansDAO;
import com.Connect.models.Payments_Model;
import com.Connect.models.Plans_Model;
import com.Connect.models.Users_Model;

/**
 * Servlet implementation class Signup
 */
@WebServlet("/Signup")
public class Signup extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Signup() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
		System.out.println("DoGet Signup");
		getEmailPlanUser(request, response);
		getPlans(request, response);
		RequestDispatcher dispatcher = request.getRequestDispatcher("signup.jsp");	
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		insertUser(request, response);
		insertPayment(request, response);
		sendEmail(request, response);
		setActiveUser(request, response);
		
	}

	protected void insertUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Users_Model> usersList = null;
		Users_Model newUser = null;
		
		String firstName = request.getParameter("name");
		String lastName = request.getParameter("lastname");
		String email = request.getParameter("email");	
		String password = request.getParameter("password");
		String userType = request.getParameter("usertype");
		int Id_Plan = Integer.parseInt(request.getParameter("plantype"));
		System.out.println(Id_Plan + "Id Plan");
		String country = request.getParameter("country");
		String cardnumber = request.getParameter("cardnumber");
		int month = Integer.parseInt(request.getParameter("month"));
		int year = Integer.parseInt(request.getParameter("year"));
		String securityCode = request.getParameter("securitycode");
		String cardholder = request.getParameter("cardholder");
		
		newUser = new Users_Model(Id_Plan, firstName, lastName, email, password, userType, country,
				cardnumber, month, year, securityCode, cardholder
		);
		
		try {
			UsersDAO.insertUpdateDeleteUsers("I",newUser);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		invalidateEmail(request, response);
		
		
	}
	
	protected void insertPayment(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Users_Model> usersList = null;
		Payments_Model newPayment = null;
		
		usersList = getUser(request, response);
		
		if(usersList.get(0) != null){
 
			long Id_User = usersList.get(0).getId_User();
			int Id_Plan = usersList.get(0).getId_Plan();
			
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(new java.util.Date());
			calendar.add(Calendar.DATE, 15);
			
			Date BeginDatePayment = calendar.getTime();
			Date CutoffDatePayment = null;
			
			switch (usersList.get(0).getTypePlan()) {
	            case "Mensual":
	    			calendar.add(Calendar.MONTH, 1);
	    			CutoffDatePayment = calendar.getTime();
	    			break;
	            case "Anual":
	            	calendar.add(Calendar.YEAR, 1);
	            	CutoffDatePayment = calendar.getTime();
	            	break;
	            default: 
	            	System.out.println("Tipo de plan mal definido en la bd");  
			}
			
//			System.out.println(usersList.get(0).getPricePlan()); //Para checar como lo trae
			BigDecimal AmountPayment = new BigDecimal(0);
			if(usersList.get(0).getTypePlan().equals("Mensual")) {
				AmountPayment = usersList.get(0).getPricePlan();
			}
			else if(usersList.get(0).getTypePlan().equals("Anual")) {
				AmountPayment = usersList.get(0).getPricePlan().multiply(new BigDecimal(12));
			}
			
			System.out.println(AmountPayment);
			
			newPayment = new Payments_Model(Id_User, Id_Plan, BeginDatePayment, 
					CutoffDatePayment, AmountPayment);
			
			try {
				PaymentsDAO.insertUpdateDeletePayments("I", newPayment);
			} catch (Exception e) {
				e.printStackTrace();
			}
			

			System.out.println(BeginDatePayment); //El dia de hoy mas 15
			System.out.println(CutoffDatePayment);
			
		}
		else
			System.out.println("No se trajo ningun usuario");

	}
	
	private void setActiveUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		List<Users_Model> UserList = getUser(request, response);
		
		if (UserList.isEmpty() == false) {
        	request.getSession().setAttribute("ActiveUserId", UserList.get(0).getId_User());
        	response.sendRedirect("DashGeneral");
	     }
         else {
        	System.out.println("No hay");
        	response.sendRedirect("login.jsp");
         }
	
	}
	
	private List<Users_Model> getUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Users_Model> usersList = null;

		Users_Model selectedUser = null;
        
        String emailUser = request.getParameter("email");
        String passwordUser = request.getParameter("password");
        
        System.out.println(emailUser);
		System.out.println(passwordUser);
        
        selectedUser = new Users_Model(emailUser, passwordUser);
        
        try {
        	usersList = UsersDAO.getUsers("L", selectedUser);
		} catch (Exception e) {
			e.printStackTrace();
		}
        
        return usersList;
		
		
	}

	private void getEmailPlanUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getSession().getAttribute("UserEmail") != null) {
			String email = (String) request.getSession().getAttribute("UserEmail");
			request.setAttribute("UserEmail", email);
			System.out.println(email);
		}
		if(request.getParameter("plan") != null) {
			int Idplan = Integer.parseInt((String) request.getParameter("plan"));
			request.setAttribute("plan", Idplan);
			System.out.println(Idplan);
		}

	}
	
	private void getPlans(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Plans_Model> planList = null;
        
        try {
        	planList = PlansDAO.getPlans("X", new Plans_Model());
		} catch (Exception e) {
			e.printStackTrace();
		}
       
        request.setAttribute("PlanList", planList);
	}

	private void invalidateEmail(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		if(request.getSession().getAttribute("UserEmail") != null)
			request.getSession().setAttribute("UserEmail", null);
	}

	public void sendEmail(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Properties properties = new Properties();
		
		String emailUser = request.getParameter("email");
		
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.host", "smtp.gmail.com");
		properties.put("mail.smtp.port", "587");
		
		String myAccountemail = "connectJosueAnimations@gmail.com";
		String password = "connectJosue123";
		
		Session session = Session.getInstance(properties, new Authenticator(){
			
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(myAccountemail,password);
			}
			
		});
		
		Message message = prepareMessage(session, myAccountemail, emailUser);
		
		try {
			Transport.send(message);
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private Message prepareMessage(Session session, String myAccountemail, String recepient){
		
		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(myAccountemail));
			message.setRecipient(Message.RecipientType.TO, new InternetAddress(recepient));
			message.setSubject("Bienvenido a Connect by Josue Animations");
			message.setContent("<div><div><div style=\"background: #17D8E2; font-size: 2em; color: #fff; font-family: 'Montserrat', sans-serif; font-weight: 500; "
					+ "padding: 30px;\"><p style=\"text-align: center;\">Bienvenido a Connect by Josu� Animations</p></div></div><div><div style=\"background: #212529; "
					+ "font-size: 2em; color: #fff; font-family: 'Montserrat', sans-serif;font-weight: 300; padding: 30px;\"><div><p style=\"text-align: center;\">"
					+ "Ha v�nculado su correo electr�nico con una cuenta de nuestros servicios</p><p style=\"text-align: center;\">�Le agradecemos su preferencia!</p>"
					+ "<p style=\"font-size: 1em; text-align: center;\"> Para iniciar la creaci�n de su sitio web inicie sesi�n en nuestra p�gina:</p><p style=\"text-align: center;\">"
					+ "<a href=\"http://localhost:8080/Connect/index.jsp\" style=\"color: #fff;\">www.Connect.com</a></p></div></div></div></div>", "text/html");
			
			return message;
			
		} catch (AddressException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

	
}
