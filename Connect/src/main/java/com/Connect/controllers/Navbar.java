package com.Connect.controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.Connect.dao.UsersDAO;
import com.Connect.models.Users_Model;

/**
 * Servlet implementation class Navbar
 */
@WebServlet("/Navbar")
public class Navbar extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Navbar() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}


	public static Users_Model getUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		List<Users_Model> usersList = null;
		Users_Model selectedUser = null;

		if (request.getSession().getAttribute("ActiveUserId") != null) {
			long IdUsuarioActivo = (Long) request.getSession().getAttribute("ActiveUserId");
			
			selectedUser = new Users_Model(IdUsuarioActivo);		
			
			try {
				usersList = UsersDAO.getUsers("S", selectedUser);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if (usersList != null)
			return usersList.get(0);
		else
			return null;
	}
	
}
