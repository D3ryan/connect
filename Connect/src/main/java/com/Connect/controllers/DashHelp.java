package com.Connect.controllers;

import java.io.IOException;
import java.util.List;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.swing.JOptionPane;

import com.Connect.dao.UsersDAO;
import com.Connect.models.Users_Model;

/**
 * Servlet implementation class DashHelp
 */
@WebServlet("/DashHelp")
public class DashHelp extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DashHelp() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//Aqui me traere el email
		request.setCharacterEncoding("UTF-8");
		sendEmail("connectJosueAnimations@gmail.com", request, response);
//		response.sendRedirect("dash-help.jsp");
	}
	
	public List<Users_Model> getUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Users_Model> selectedUser = null;
		Users_Model auxUser = null;
		
		long IdActiveUser = -1;
		
		if (request.getSession().getAttribute("ActiveUserId") != null) {
			IdActiveUser = (Long) request.getSession().getAttribute("ActiveUserId");
			
			auxUser = new Users_Model(IdActiveUser);
			
			try {
				selectedUser = UsersDAO.getUsers("S", auxUser);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return selectedUser;
		
	}
	
	public void sendEmail(String recepient, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Users_Model> selectedUser = getUser(request, response);
		
		Properties properties = new Properties();
		
		String subject = request.getParameter("subject");
		String comment = request.getParameter("comment");
		
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.host", "smtp.gmail.com");
		properties.put("mail.smtp.port", "587");
		
		String myAccountemail = "connectJosueAnimations@gmail.com";
		String password = "connectJosue123";
		
		Session session = Session.getInstance(properties, new Authenticator(){
			
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(myAccountemail,password);
			}
			
		});
		
		Message message = prepareMessage(session, myAccountemail, recepient, subject, comment, selectedUser);
		
		try {
			Transport.send(message);
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	private Message prepareMessage(Session session, String myAccountemail, String recepient,  String subject, String comment, List<Users_Model> user){
		
		subject += " Soporte";
		comment += "\nCorreo de Usuario: " + user.get(0).getEmailUser();
		
		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(myAccountemail));
			message.setRecipient(Message.RecipientType.TO, new InternetAddress(recepient));
			message.setSubject(subject);
			message.setText(comment);
		
			return message;
			
		} catch (AddressException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}


}
