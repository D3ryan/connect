package com.Connect.controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.Connect.dao.PlansDAO;
import com.Connect.models.Plans_Model;

/**
 * Servlet implementation class Plans
 */
@WebServlet("/Plans")
public class Plans extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Plans() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("Do get Plans");
		getPlans(request, response);
		RequestDispatcher dispatcher = request.getRequestDispatcher("plans.jsp");	
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("Do post Plans");
	}
	
	private void getPlans(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Plans_Model> planList = null;
        
        try {
        	planList = PlansDAO.getPlans("X", new Plans_Model());
		} catch (Exception e) {
			e.printStackTrace();
		}
        request.setAttribute("PlanList", planList);
	}
	
}
