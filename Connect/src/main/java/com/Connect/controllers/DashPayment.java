package com.Connect.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.Connect.dao.PaymentsDAO;
import com.Connect.dao.UsersDAO;
import com.Connect.models.Payments_Model;
import com.Connect.models.Users_Model;

/**
 * Servlet implementation class DashPayment
 */
@WebServlet("/DashPayment")
public class DashPayment extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DashPayment() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		getUser(request, response);
		getPayments(request,response);
		RequestDispatcher dispatcher = request.getRequestDispatcher("dash-payment.jsp");	
		dispatcher.forward(request, response);
	}

	/**`
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		getDatePayments(request,response);
   
//		RequestDispatcher dispatcher = request.getRequestDispatcher("dash-payment.jsp");	
//		dispatcher.forward(request, response);
	}
	
	public void getPayments(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		List<Payments_Model> Payments = null;
		Payments_Model auxPayment = null;
		
		long IdActiveUser = -1;
		
		if (request.getSession().getAttribute("ActiveUserId") != null) {
			
			IdActiveUser = (Long) request.getSession().getAttribute("ActiveUserId");
			auxPayment = new Payments_Model(IdActiveUser);
			
			try {
				
				Payments = PaymentsDAO.getPayments("UP", auxPayment);
				System.out.println(Payments.get(0).getAmountPayment());
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
			request.setAttribute("PaymentsUser", Payments);
		}
			
	}
	
	public void getDatePayments(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		List<Payments_Model> Payments = null;
		Payments_Model auxPayment = null;
		
		PrintWriter out=response.getWriter();
        response.setContentType("text/html");
        
        String html = "";
        
		long IdActiveUser = -1;
		
		if (request.getSession().getAttribute("ActiveUserId") != null) {			
			
			IdActiveUser = (Long) request.getSession().getAttribute("ActiveUserId");
			
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd"); 
 
			String year = request.getParameter("year");
			String month = request.getParameter("month");
			String dummy = request.getParameter("dummy");
			String stringDate = year + "-" + month + "-" + "01";			
			System.out.println(stringDate);
			
			try {
				System.out.println(formatter.parse(stringDate));
				Date BeginDatePayment = formatter.parse(stringDate);
				auxPayment = new Payments_Model(IdActiveUser, BeginDatePayment);
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			
			try {
				
				Payments = PaymentsDAO.getPayments("DUP", auxPayment);
				System.out.println(Payments.get(0).getAmountPayment());
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			for(Payments_Model payment : Payments) {
				html += "<p class='df_payment df_text'> " + payment.getBeginDatePayment() + " a " + payment.getCutoffDatePayment() + " Plan " + payment.getNamePlan() + " " + payment.getTypePlan() +
						" $ " + payment.getAmountPayment() + " USD";
				
				if(payment.getStatusPayment() == false) {
					html += " Sin cobrar" + " </p>";
				}
				else
					html += " </p>";
			}
			
			if(Payments.isEmpty())
				html += "<p class='df_payment df_text'>No hay pagos con el periodo indicado</p>";
			
			out.print(html);
			
			//request.setAttribute("PaymentsUser", Payments);
		}
	}
	
	public void getUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		
		List<Users_Model> selectedUser = null;
		Users_Model auxUser = null;
		
		long IdActiveUser = -1;
		
		if (request.getSession().getAttribute("ActiveUserId") != null) {
			IdActiveUser = (Long) request.getSession().getAttribute("ActiveUserId");
			auxUser = new Users_Model(IdActiveUser);
			
			try {
				selectedUser = UsersDAO.getUsers("DH", auxUser);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		request.setAttribute("SelectedUser", selectedUser.get(0));
		request.setAttribute("ActiveUserId", IdActiveUser);
		
	}

}
