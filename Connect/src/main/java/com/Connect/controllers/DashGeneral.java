package com.Connect.controllers;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.Connect.dao.PaymentsDAO;
import com.Connect.dao.PlansDAO;
import com.Connect.dao.UsersDAO;
import com.Connect.models.Payments_Model;
import com.Connect.models.Plans_Model;
import com.Connect.models.Users_Model;

/**
 * Servlet implementation class DashGeneral
 */
@WebServlet("/DashGeneral")
public class DashGeneral extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DashGeneral() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		checkPayments(request, response);
		getUser(request, response);
		RequestDispatcher dispatcher = request.getRequestDispatcher("dash-general.jsp");	
		dispatcher.forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		doGet(request, response);
	}
	
	public void getUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Users_Model> selectedUser = null;
		Users_Model auxUser = null;
		
		long IdActiveUser = -1;
		
		if (request.getSession().getAttribute("ActiveUserId") != null) {
			IdActiveUser = (Long) request.getSession().getAttribute("ActiveUserId");
			
			auxUser = new Users_Model(IdActiveUser);
			
			try {
				selectedUser = UsersDAO.getUsers("DH", auxUser);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		request.setAttribute("SelectedUser", selectedUser.get(0));
		request.setAttribute("ActiveUserId", IdActiveUser);

		setPlanPromo(request, response, selectedUser.get(0));
		
		
	}
	

	public void setPlanPromo(HttpServletRequest request, HttpServletResponse response, Users_Model selectedUser) throws ServletException, IOException {
		List<Plans_Model> Better_Plan = null;
		Plans_Model auxPlan = new Plans_Model(selectedUser.getId_Plan());
		
		try {
			
			Better_Plan = PlansDAO.getPlans("BP", auxPlan);
			
			if(Better_Plan.isEmpty() == false) {
				System.out.println("Es aplicable");
				request.setAttribute("PromoType", "BP"); //Mas comprado
			}
			else {
				System.out.println("Ya tiene el mas comprado mostremos 3 mas");
				Better_Plan = PlansDAO.getPlans("NP", auxPlan);
				if(Better_Plan.isEmpty() == false) 
					request.setAttribute("PromoType", "N3"); //3, 2 o 1 planes siguientes
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}		
		
		if(Better_Plan.isEmpty() == false) {
			System.out.println(Better_Plan.get(0).getId_Plan());
			request.setAttribute("PromoPlans", Better_Plan);
		}
		
	}

}

//DEPRECATED CODE

//private void checkPayments(HttpServletRequest request, HttpServletResponse response) {
//
//	
//	List<Payments_Model> userLastPayment = null;
//	Payments_Model payment = null;
//	
//	Calendar date = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
//	date.set(2022, 5, 25);
//	 
//	
//	long IdActiveUser = -1;
//	
//	if (request.getSession().getAttribute("ActiveUserId") != null) {
//		IdActiveUser = (Long) request.getSession().getAttribute("ActiveUserId");
//		payment = new Payments_Model(IdActiveUser);
//		
//		
//			try {
//				userLastPayment = PaymentsDAO.getPayments("CPY", payment);
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//			
//			Calendar BeginDatePayment = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
//			Calendar CutoffDatePayment = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
//			BeginDatePayment.setTime(userLastPayment.get(0).getBeginDatePayment());
//			CutoffDatePayment.setTime(userLastPayment.get(0).getCutoffDatePayment());
//			
//			if(userLastPayment.get(0).getStatusPayment() == false) {
//				if(BeginDatePayment.compareTo(date) == 0 || BeginDatePayment.compareTo(date) < 0) {
//					updateFirstPayment(request, response, userLastPayment);
//				}
//			}
//			else {
//				if(CutoffDatePayment.compareTo(date) == 0 || CutoffDatePayment.compareTo(date) < 0) {
//					insertPayment(request, response, userLastPayment);
//				}
//			}
//			
//	}
	
	
	
//}

//private void updateFirstPayment(HttpServletRequest request, HttpServletResponse response, List<Payments_Model> userLastPayment) {
//
//	userLastPayment.get(0).setStatusPayment(true);
//	try {
//		PaymentsDAO.insertUpdateDeletePayments("U", userLastPayment.get(0));
//	} catch (Exception e) {
//		e.printStackTrace();
//	}
//	
//}

//private void insertPayment(HttpServletRequest request, HttpServletResponse response, List<Payments_Model> userLastPayment) {
//	Payments_Model newPayment = null;
//	
//	long Id_User = userLastPayment.get(0).getId_User();
//	int Id_Plan = userLastPayment.get(0).getId_Plan();
//	System.out.println(Id_User);
//	System.out.println(Id_Plan);
//	Calendar calendar = Calendar.getInstance();
//	calendar.setTime(userLastPayment.get(0).getCutoffDatePayment());
//	
//	Date BeginDatePayment = calendar.getTime();
//	System.out.println(BeginDatePayment);
//	Date CutoffDatePayment = null;
//	
//	switch (userLastPayment.get(0).getTypePlan()) {
//        case "Mensual":
//			calendar.add(Calendar.MONTH, 1);
//			CutoffDatePayment = calendar.getTime();
//			break;
//        case "Anual":
//        	calendar.add(Calendar.YEAR, 1);
//        	CutoffDatePayment = calendar.getTime();
//        	break;
//        default: 
//        	System.out.println("Tipo de plan mal definido en la bd");  
//	
//	}
//	
//	System.out.println(userLastPayment.get(0).getPricePlan());
//	
//	BigDecimal PricePlan = userLastPayment.get(0).getPricePlan();
//	
//	newPayment = new Payments_Model(Id_User, Id_Plan, BeginDatePayment, 
//			CutoffDatePayment, PricePlan, true);
//	
//	try {
//		PaymentsDAO.insertUpdateDeletePayments("I", newPayment);
//	} catch (Exception e) {
//		e.printStackTrace();
//	}
//	
//	System.out.println(CutoffDatePayment);
//}
