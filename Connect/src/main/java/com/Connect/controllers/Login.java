package com.Connect.controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.Connect.dao.UsersDAO;
import com.Connect.models.Users_Model;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		invalidateUser(request, response);
		response.sendRedirect("index.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub		
		List<Users_Model> UserList = getUser(request, response);
		
		 if (UserList.isEmpty() == false) {
	        	request.getSession().setAttribute("ActiveUserId", UserList.get(0).getId_User());
	        	response.sendRedirect("DashGeneral");
	        }
	        else {
	        	request.setAttribute("userFound", "false");
	        	RequestDispatcher dispatcher = request.getRequestDispatcher("login.jsp");	
	    		dispatcher.forward(request, response);
	        }
		
	}
	
	private List<Users_Model> getUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		List<Users_Model> usersList = null;
		
		Users_Model selectedUser = null;
        
        String emailUser = request.getParameter("email");
        String passwordUser = request.getParameter("password");
        
        selectedUser = new Users_Model(emailUser, passwordUser);
        
        try {
        	usersList = UsersDAO.getUsers("L", selectedUser);
		} catch (Exception e) {
			e.printStackTrace();
		}
        
        return usersList;
	}
	
	private void invalidateUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		if (request.getSession().getAttribute("ActiveUserId") != null)
			request.getSession().invalidate();
			
	}
}
